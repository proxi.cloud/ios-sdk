//
//  PCEvent.m
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCEvent.h"

#import "ProxicloudSDK.h"

#import <CoreBluetooth/CoreBluetooth.h>

#import <UserNotifications/UserNotifications.h>

#import "PCUtilities.h"

emptyImplementation(PCEvent)

#pragma mark - Protocol events

@implementation PCEventPerformAction

- (NSDictionary *)toDictionary {
    NSMutableDictionary *ret = [NSMutableDictionary new];
    if (self.campaign.fireDate) {
        [ret setValue:[NSNumber numberWithDouble:[self.campaign.fireDate timeIntervalSince1970]] forKey:@"fireDate"];
    } else {
        [ret setValue:[NSNumber numberWithDouble:[[NSDate dateWithTimeIntervalSinceNow:1] timeIntervalSince1970]] forKey:@"fireDate"];
    }
    [ret setValue:self.campaign.subject forKey:@"subject"];
    [ret setValue:self.campaign.body forKey:@"body"];
    if (self.campaign.payload) {
        [ret setValue:self.campaign.payload forKey:@"payload"];
    }
    if (self.campaign.url) {
        [ret setValue:self.campaign.url forKey:@"url"];
    }
    [ret setValue:self.campaign.eid forKey:@"eid"];
    [ret setValue:[NSNumber numberWithInteger:self.campaign.trigger] forKey:@"trigger"];
    [ret setValue:[NSNumber numberWithInteger:self.campaign.type] forKey:@"type"];
    [ret setValue:self.campaign.action forKey:@"action"];
    return ret;
}

@end

emptyImplementation(PCEventPerformWhenInUseAction)

emptyImplementation(PCEventResetManager)

emptyImplementation(PCEventReportHistory)

emptyImplementation(PCEventReportConversion)

emptyImplementation(PCEventUpdateHeaders)

emptyImplementation(PCEventStatusUpdate)

#pragma mark - Location events

emptyImplementation(PCEventRangedBeacon)

emptyImplementation(PCEventRegionEnter)

emptyImplementation(PCEventRegionExit)

#pragma mark - Authorization events

emptyImplementation(PCEventLocationAuthorization)

emptyImplementation(PCEventBluetoothAuthorization)

emptyImplementation(PCEventNotificationsAuthorization)

#pragma mark - WiFi events

emptyImplementation(PCEventWiFiStatus)

emptyImplementation(PCEventDeviceConnectedToWiFiNetwork)

emptyImplementation(PCEventDeviceDisconnectedFromWiFiNetwork)

#pragma mark - Custom event

emptyImplementation(PCEventCustom)

#pragma mark - AID events

emptyImplementation(PCEventAID)

#pragma mark - CoreBluetooth events

emptyImplementation(PCEventBluetoothEmulation)

emptyImplementation(PCEventDeviceDiscovered)

emptyImplementation(PCEventDeviceUpdated)

emptyImplementation(PCEventDeviceDisconnected)

emptyImplementation(PCEventDeviceConnected)

emptyImplementation(PCEventServicesUpdated)

emptyImplementation(PCEventCharacteristicsUpdate)

emptyImplementation(PCEventCharacteristicWrite)

#pragma mark - Magnetometer events

emptyImplementation(PCEventMagnetometerUpdate)

#pragma mark - Application life-cycle events

emptyImplementation(PCEventApplicationLaunched)

emptyImplementation(PCEventApplicationActive)

emptyImplementation(PCEventApplicationForeground)

emptyImplementation(PCEventApplicationWillResignActive)

emptyImplementation(PCEventApplicationWillTerminate)

emptyImplementation(PCEventApplicationWillEnterForeground)

emptyImplementation(PCEventApplicationDidEnterBackground)

#pragma mark - Resolver events

emptyImplementation(PCEventReachabilityEvent)

emptyImplementation(PCEventUpdateResolver)

#pragma mark - PCSettings events

emptyImplementation(PCUpdateSettingEvent);

emptyImplementation(PCSettingEvent);
