//
//  PCManager.m
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "ProxicloudSDK.h"
@import UserNotifications;

#import "PCManager.h"

#import "PCResolver.h"
#import "PCLocation.h"
#import "PCAnalytics.h"
#import "PCSettings.h"
#import "PCMagnetometer.h"
#import "PCWifiManager.h"
#import "PCAIDManager.h"
#import "PCInternalEvents.h"
#import <AdSupport/ASIdentifierManager.h>

#import "PCUtility.h"
#import "PCSettings.h"
#import "NSString+PCUUID.h"

#import <UICKeyChainStore/UICKeyChainStore.h>

#import <UIKit/UIKit.h>

#import <tolo/Tolo.h>

#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <FirebaseMessaging/FIRMessaging.h>

#pragma mark - PCManager

@interface PCManager () {
    //
    double ping;
    //
    double delay;
    
    PCResolver                  *apiClient;
    PCLocation                  *locClient;
    PCBluetooth                 *bleClient;
    PCAnalytics                 *anaClient;
    PCMagnetometer              *magClient;
    PCWifiManager               *wifiClient;
    PCAIDManager                *aidManager;
    PCMGetLayout                *layout;
    UNUserNotificationCenter    *center;
    UIBackgroundTaskIdentifier  backgroundTask;
    NSTimer                     *timer;
    NSDictionary                *targetAttributes;
}

@end

@implementation PCManager

NSString *PCAPIKey = nil;
NSString *PCResolverURL = nil;


static PCManager * _sharedManager;

static dispatch_once_t once;

+ (instancetype)sharedManager {
    if (!_sharedManager) {
        //
        dispatch_once(&once, ^ {
            _sharedManager = [[self alloc] init];
        });
        //
    }
    return _sharedManager;
}

- (void)resetSharedClient {
    // enforce main thread
    if (![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resetSharedClient];
        });
        return;
    }
    //
    [self stopMonitoring];
    //
    PCResolverURL = nil;
    //
    PCAPIKey = nil;
    //
    _sharedManager = nil;
    // we reset the dispatch_once_t to 0 (it's a long) so we can re-create the singleton instance
    once = 0;
    // we also reset the latency value to -1 (no connectivity)
    ping = -1;
    //
    [keychain removeAllItems];
    keychain = nil;
    //
    UNREGISTER();
    [[Tolo sharedInstance] unsubscribe:anaClient];
    [[Tolo sharedInstance] unsubscribe:apiClient];
    [[Tolo sharedInstance] unsubscribe:locClient];
    [[Tolo sharedInstance] unsubscribe:bleClient];
    [[Tolo sharedInstance] unsubscribe:magClient];
    [[Tolo sharedInstance] unsubscribe:wifiClient];
    [[Tolo sharedInstance] unsubscribe:aidManager];
    //
    anaClient = nil;
    apiClient = nil;
    locClient = nil;
    bleClient = nil;
    wifiClient = nil;
    aidManager = nil;
    //
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //
    PUBLISH([PCEventResetManager new]);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //
        if (isNull(locClient)) {
            locClient = [PCLocation new];
            [[Tolo sharedInstance] subscribe:locClient];
        }
        //
        if (isNull(bleClient)) {
            bleClient = [PCBluetooth sharedManager];
            [[Tolo sharedInstance] subscribe:bleClient];
        }
        //
        if (isNull(anaClient)) {
            anaClient = [PCAnalytics new];
            [[Tolo sharedInstance] subscribe:anaClient];
        }
        //
        if (isNull(magClient)) {
            magClient = [PCMagnetometer new];
            [[Tolo sharedInstance] subscribe:magClient];
        }
        //
        center = [UNUserNotificationCenter currentNotificationCenter];
        //
        REGISTER();
        // set the latency to a negative value before the first health check
        ping = -1;
        [apiClient ping];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunchingWithOptions:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        //
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
        //
        [[FIRMessaging messaging] subscribeToTopic:@"proxicloud"
                                        completion:^(NSError * _Nullable error) {
          NSLog(@"Subscribed to Proxi Cloud default topic");
        }];
    }
    return self;
}

#pragma mark - Designated initializer

- (void)setApiKey:(NSString*)apiKey delegate:(id)delegate {
    if ([NSThread currentThread]!=[NSThread mainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setApiKey:apiKey delegate:delegate];
        });
        return;
    }
    //
#ifndef DEBUG
    if ([self availabilityStatus]==PCManagerAvailabilityStatusIBeaconUnavailable) {
        // fire error event
        return;
    }
#endif

    // if apiKey is changed, reset Settings.
    if (!apiKey.length || [apiKey isEqualToString:(PCAPIKey ? PCAPIKey : @"")])
    {
        [[PCSettings sharedManager] reset];
    }
    //
    keychain = [UICKeyChainStore keyChainStoreWithService:kPCIdentifier];
    //
    keychain.accessibility = UICKeyChainStoreAccessibilityAlways;
    keychain.synchronizable = YES;
    //
    PCAPIKey = apiKey.length ? apiKey : kPCDefaultAPIKey;
    //
    if (isNull(apiClient)) {
        apiClient = [[PCResolver alloc] initWithApiKey:PCAPIKey];
        [[Tolo sharedInstance] subscribe:apiClient];
        
        // publish event
        PCEventUpdateTargetAttributes *event = [PCEventUpdateTargetAttributes new];
        event.targetAttributes = targetAttributes;
        PUBLISH(event);
    }
    if (isNull(aidManager)) {
        aidManager = [[PCAIDManager alloc] initWithResolver:apiClient];
    }
    if (isNull(wifiClient)) {
        wifiClient = [[PCWifiManager alloc] initWithResolver: apiClient];
        [[Tolo sharedInstance] subscribe:wifiClient];
    }
    //
    if (!isNull(delegate)) {
        [[Tolo sharedInstance] subscribe:delegate];
    }
    //
    [apiClient requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:NO];
    //
    [apiClient requestSettingsWithAPIKey:PCAPIKey];
    //
    
    [self reportDeliveredFcmNotifications];
    
    PCLog(@"👍 Proxi Cloud SDK [%@]",[PCUtility userAgent].sdk);
}

#pragma mark - Resolver methods

- (NSString *)resolverURL
{
    return [PCSettings sharedManager].settings.resolverURL;
}

- (double)resolverLatency {
    return ping;
}

- (void)requestResolverStatus {
    [apiClient ping];
}

SUBSCRIBE(PCEventPing) {
    if (event.error) {
        return;
    }
    ping = event.latency;
}

#pragma mark - Location methods

- (void)requestLocationAuthorization {
    [self requestLocationAuthorization:YES];
}

- (void)requestLocationAuthorization:(BOOL)always {
    if (locClient) {
        [locClient requestAuthorization:always];
    }
}

- (PCLocationAuthorizationStatus)locationAuthorization {
    return [locClient authorizationStatus];
}

#pragma mark - Bluetooth methods

- (void)requestBluetoothAuthorization {
    [bleClient requestAuthorization];
}

- (PCBluetoothStatus)bluetoothAuthorization {
    return [bleClient authorizationStatus];
}


#pragma mark - Analitics custom report

-(void)reportEvent:(NSString *)key withValue:(NSString *)value {
    PUBLISH(({
        PCEventCustom *event = [PCEventCustom new];
        event.key = key;
        event.value = value;
        event;
    }));
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    NSLog(@"didReceiveNotificationResponse");
    NSString *action = nil;
    if (response.notification.request.content.userInfo) {
        if ( [response.notification.request.content.userInfo valueForKey:@"locationNotification"]){
            action = [PCEnums getActionUuid: response.notification.request.content.userInfo];
            if (action){
            PCMCampaignAction *campaign = [PCMCampaignAction new];
                campaign.eid = [response.notification.request.content.userInfo valueForKey:@"eid"];
                campaign.action = [response.notification.request.content.userInfo valueForKey:@"action"];
                NSString *triggetType = [response.notification.request.content.userInfo valueForKey:@"trigger"];
                if ([triggetType  isEqual: @"1"]){
                    campaign.trigger = kPCTriggerEnter;
                } else {
                    campaign.trigger = kPCTriggerExit;
                }
                campaign.beacon = [PCMTrigger new];
                campaign.beacon.tid = [response.notification.request.content.userInfo valueForKey:@"tid"];

            PUBLISH((({
                PCEventPerformWhenInUseAction *event = [PCEventPerformWhenInUseAction new];
                event.campaign = campaign;
                event;
            })));

            [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
        }
    }
        
    }
}

#pragma mark - Firebase notification

- (PCMCampaignAction*) actionFromUserInfo:(NSDictionary *) userInfo {
    PCMCampaignAction *action = [PCMCampaignAction new];
    action.fireDate = [NSDate date];
    id object = [userInfo objectForKey:@"subject"];
    if (object) {
        action.subject = object;
    }
    object = [userInfo objectForKey:@"body"];
    if (object) {
        action.body = object;
    }
    NSDictionary *dict = [userInfo objectForKey:@"payload"];
    if (dict) {
        action.payload = [NSDictionary new];
    }
    object = [userInfo objectForKey:@"url"];
    if (object) {
        action.url = object;
    }
    object = [userInfo objectForKey:@"eid"];
    if (object) {
        action.eid = object;
    }
    object = [userInfo valueForKey:@"action_type"];
    if (object) {
        action.type = [object integerValue];
    }
    object = [userInfo objectForKey:@"trigger_id"];
    if (object) {
        PCMTrigger *trigger = [PCMTrigger new];
        trigger.tid = object;
        action.beacon = trigger;
    }
    action.action = [userInfo valueForKey:@"action"];
    return action;
}

-(void)passFcmToPcSdk:(NSDictionary *)userInfo{
    NSString *const kGCMMessageIDKey = @"gcm.message_id";
    if (userInfo[kGCMMessageIDKey]) {
        PCMCampaignAction *action = [self actionFromUserInfo:userInfo];
        PUBLISH((({
            PCEventInternalAction *event = [PCEventInternalAction new];
            event.campaign = action;
            event;
        })));
        
        [self reportConversion: kPCConversionSuccessful forCampaignAction:action.action];
    }
}

- (void)populateNotificationContent:(UNNotificationContent *)content
                 withContentHandler:(void (^)(UNNotificationContent *_Nonnull))contentHandler {
    NSMutableDictionary *mutableUserInfo = [content.userInfo mutableCopy];
    UNMutableNotificationContent *mutableContent = [content mutableCopy];
    [mutableUserInfo setObject:[NSUUID UUID].UUIDString forKey:@"action"];
    [mutableUserInfo setObject:@"proxi_fcm" forKey:@"sender_id"];
    mutableContent.userInfo = mutableUserInfo;
    contentHandler(mutableContent);
}

- (void) reportDeliveredFcmNotifications {
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for (UNNotification* notification in notifications)
        {
            NSString *senderId = [notification.request.content.userInfo valueForKey:@"sender_id"];
            if( [senderId  isEqual: @"proxi_fcm"]) {
                PCMCampaignAction *action = [self actionFromUserInfo:notification.request.content.userInfo];
                action.fireDate = notification.date;
                PUBLISH((({
                    PCEventInternalAction *event = [PCEventInternalAction new];
                    event.campaign = action;
                    event;
                })));
                [self reportConversion: kPCConversionIgnored forCampaignAction:action.action];
            }
        }
    }];
}

- (void) passResponseToPcSdkForNotification: (UNNotification *) notification {
    if ([notification.request.content.userInfo valueForKey:@"sender_id"]){
        PCMCampaignAction *action = [self actionFromUserInfo:notification.request.content.userInfo];
        action.fireDate = notification.date;
        PUBLISH((({
            PCEventInternalAction *event = [PCEventInternalAction new];
            event.campaign = action;
            event;
        })));
        [self reportConversion: kPCConversionSuccessful forCampaignAction:action.action];
    }
}


#pragma mark - Notifications

-(void)requestNotificationsAuthorization {
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionBadge + UNAuthorizationOptionSound;
    [center requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
        NSLog(@"Is authorization granted:%d", granted);
    }];
}

-(void)isEnabledToReceiveNotificationsWithCompletion:(void(^)(BOOL))completionBlock {
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        
        if (completionBlock) {
            BOOL status = settings.authorizationStatus == UNAuthorizationStatusAuthorized;dispatch_async(dispatch_get_main_queue(), ^{
                BOOL notifs = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
                completionBlock(status||notifs);
            });
        }
    }];
}

- (BOOL)canReceiveNotifications {
    UIUserNotificationSettings *notificationSettings =  [[UIApplication sharedApplication] currentUserNotificationSettings];
    
    BOOL status = (notificationSettings.types & UIUserNotificationTypeSound) || (notificationSettings.types & UIUserNotificationTypeAlert) || (notificationSettings.types & UIUserNotificationTypeBadge);
    
    BOOL notifs = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    
    return status||notifs;
}


#pragma mark - Status

- (PCManagerAvailabilityStatus)availabilityStatus {
    //
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        return PCManagerAvailabilityStatusIBeaconUnavailable; // not possible
    }
    //
    switch (bleClient.authorizationStatus) {
        case PCBluetoothOff: {
            return PCManagerAvailabilityStatusBluetoothRestricted;
        }
        default: {
            break;
        }
    }
    //
    switch (self.backgroundAppRefreshStatus) {
        case PCManagerBackgroundAppRefreshStatusRestricted:
        case PCManagerBackgroundAppRefreshStatusDenied:
            return PCManagerAvailabilityStatusBackgroundAppRefreshRestricted;
            
        default:
            break;
    }
    //
    switch (locClient.authorizationStatus) {
        case PCLocationAuthorizationStatusNotDetermined:
        case PCLocationAuthorizationStatusUnimplemented:
        case PCLocationAuthorizationStatusRestricted:
        case PCLocationAuthorizationStatusDenied:
        case PCLocationAuthorizationStatusUnavailable:
            return PCManagerAvailabilityStatusAuthorizationRestricted;
            
        default: 
            break;
    }
    
    switch (wifiClient.authorizationStatus) {
        case PCWifiOff:
        case PCWifiUnknown: {
            return PCManagerAvailabilityStatusWifiRestricted;
        }
        default :
            break;
    }
    
    //
    if (!apiClient.isConnected) {
        return PCManagerAvailabilityStatusConnectionRestricted;
    }
    //
    return PCManagerAvailabilityStatusFullyFunctional;
}

- (PCManagerBackgroundAppRefreshStatus)backgroundAppRefreshStatus {
    //
    UIBackgroundRefreshStatus status = [UIApplication sharedApplication].backgroundRefreshStatus;
    //
    switch (status) {
        case UIBackgroundRefreshStatusRestricted:
            return PCManagerBackgroundAppRefreshStatusRestricted;
            
        case UIBackgroundRefreshStatusDenied:
            return PCManagerBackgroundAppRefreshStatusDenied;
            
        case UIBackgroundRefreshStatusAvailable:
            return PCManagerBackgroundAppRefreshStatusAvailable;
            
        default:
            break;
    }
    
    return PCManagerBackgroundAppRefreshStatusAvailable;
}

- (void)startMonitoring
{
    if (isNull(layout)) {
        [self startMonitoring:@[]];
        return;
    }
    [self startMonitoring:layout.accountProximityUUIDs];
}

- (void)startMonitoring:(NSArray <NSString*>*)UUIDs {
    [locClient startMonitoring:UUIDs];
    //
}

- (void)stopMonitoring {
    [locClient stopMonitoring];
}

- (void)setIDFAValue:(NSString*)IDFA {
    if (IDFA && [IDFA isKindOfClass:[NSString class]] && IDFA.length>0) {
        [keychain setString:IDFA forKey:kIDFA];
    } else {
        [keychain removeItemForKey:kIDFA];
    }
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error == nil) {
            PCEventUpdateFirebaseToken *event = [PCEventUpdateFirebaseToken new];
            event.token = result.token;
            PUBLISH(event);
        }
    }];

    PUBLISH([PCEventUpdateHeaders new]);
}

- (void)setTargetAttributes:(NSDictionary*)attributes {
    targetAttributes = [attributes copy];
    //
    PCEventUpdateTargetAttributes *event = [PCEventUpdateTargetAttributes new];
    event.targetAttributes = attributes;
    PUBLISH(event);
}

- (void)reportConversion:(PCConversionType)type forCampaignAction:(NSString *)action {
    if (isNull(action) || ![action isKindOfClass:[NSString class]] || !action.length) {
        return;
    }
    //
    PUBLISH((({
        PCEventReportConversion *event = [PCEventReportConversion new];
        event.action = action;
        event.conversionType = type;
        event.gps = locClient.gps;
        event;
    })));
}

#pragma mark - Resolver events

#pragma mark PCEventGetLayout
SUBSCRIBE(PCEventGetLayout) {
    if (event.error) {
        PCLog(@"💀 Error reading layout (%@)",event.error.localizedDescription);
        //
        if (delay<.1f || delay>200.f) {
            delay = .1f;
        }
        delay *= 3;
        //
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self->apiClient requestLayoutForBeacon:event.beacon trigger:event.trigger useCache:YES];
        });
        //
        return;
    }
    
    if (layout && [layout.toDictionary isEqualToDictionary:event.layout.toDictionary])
    {
        return;
    }
    
    //
    PCLog(@"👍 GET layout");
    layout = event.layout;
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error == nil) {
            PCEventUpdateFirebaseToken *event = [PCEventUpdateFirebaseToken new];
            event.token = result.token;
            PUBLISH(event);
        }
    }];
    //
    if (delay>3) {
        PUBLISH([PCEventReportHistory new]);
    }
    //
    if (locClient.isMonitoring) {
        [self startMonitoring];
    }
    //
    if(!isNull(layout)){
        [wifiClient setLayout: layout ];
        [aidManager setLayout: layout ];
    }
    delay = 0.1f;
    // support for WhenInUse permission
    if (locClient.authorizationStatus == PCLocationAuthorizationStatusAuthorizedWhenInUse){
        [locClient addLocationNotificationsIn:layout.accountProximityUUIDs withActions:layout.actions];
    }
}


#pragma mark PCEventPostLayout
SUBSCRIBE(PCEventPostLayout) {
    if (event.error) {
        PCLog(@"💀 Error posting layout: %@",event.error);
        return;
    }
    //
    PCLog(@"👍 POST layout");
}

#pragma mark - Location events

#pragma mark PCEventLocationAuthorization
SUBSCRIBE(PCEventLocationAuthorization) {
    NSString * locationConsentKey = @"locationConsent";
    NSString *value = @"none";
    if (event.locationAuthorization == PCLocationAuthorizationStatusAuthorizedWhenInUse){
        value = @"whenInUse";
    } else if (event.locationAuthorization == PCLocationAuthorizationStatusAuthorized){
        value = @"always";
    }
    NSString * currentValue = [[NSUserDefaults standardUserDefaults] valueForKey:locationConsentKey];
    if (![currentValue isEqualToString:value]){
        [[NSUserDefaults standardUserDefaults]  setObject:value forKey:locationConsentKey];
        NSString * jsonToSend = [NSString stringWithFormat:@"{\"locationConsent\":\"%@\"}", value];
        [self reportEvent: locationConsentKey withValue:jsonToSend];
    }
}

#pragma mark PCEventRangedBeacons
SUBSCRIBE(PCEventRangedBeacon) {
    //
}

#pragma mark PCEventRegionEnter
SUBSCRIBE(PCEventRegionEnter) {
    PCLog(@"👀 %@",[event.beacon tid]);
    //
    [layout checkCampaignsForBeacon:event.beacon trigger:kPCTriggerEnter];
}

#pragma mark PCEventRegionExit
SUBSCRIBE(PCEventRegionExit) {
    PCLog(@"🏁 %@",[event.beacon tid]);
    //
    [layout checkCampaignsForBeacon:event.beacon trigger:kPCTriggerExit];
}

SUBSCRIBE(PCEventLocationUpdated) {
    [wifiClient fetchLayoutForEvents];
    [aidManager fetchGroupAdvertising];
    if (locClient.authorizationStatus == PCLocationAuthorizationStatusAuthorizedWhenInUse){
        [locClient addLocationNotificationsIn:layout.accountProximityUUIDs withActions:layout.actions];
    }
}

//TODO:
#pragma mark - WiFi events

#pragma mark PCEventWiFiStatus
SUBSCRIBE(PCEventWiFiStatus) {
    //TODO
}

#pragma mark PCEventDeviceConnectedToWiFiNetwork
SUBSCRIBE(PCEventDeviceConnectedToWiFiNetwork) {
    [layout checkCampaignsForBeacon:event.wifi trigger:kPCTriggerEnter]; //??
}

SUBSCRIBE(PCEventDeviceDisconnectedFromWiFiNetwork) {
    [layout checkCampaignsForBeacon:event.wifi trigger:kPCTriggerExit];
}

//TODO:
#pragma mark PCEventAID
SUBSCRIBE(PCEventAID){
    [layout checkCampaignsForBeacon:event.aid trigger:kPCTriggerNone]; //?
}


#pragma mark - Analytics
SUBSCRIBE(PCEventReportHistory) {
    if (!event.forced) {
        NSString *lastPostString = [keychain stringForKey:kPostLayout];
        if (!isNull(lastPostString)) {
            NSDate *lastPostDate = [dateFormatter dateFromString:lastPostString];
            if (!isNull(lastPostDate)) {
                if ([[NSDate date] timeIntervalSinceDate:lastPostDate] < [PCSettings sharedManager].settings.postSuppression) {
                    return;
                }
            }
        }
    }
    //
    if (anaClient.events.count || anaClient.actions.count || anaClient.conversions.count) {
        // Create postData object to send
        PCMPostLayout *postData = [PCMPostLayout new];
        postData.events = [anaClient events];
        postData.actions = [anaClient actions];
        postData.conversions = [anaClient conversions];
        postData.deviceTimestamp = [NSDate date];
        PCLog(@"❓ POST layout");
        //
        // Set lastPost timestamp
        NSString *lastPostString = [dateFormatter stringFromDate:[NSDate date]];
        [keychain setString:lastPostString forKey:kPostLayout];
        //
        [apiClient postLayout:postData];
    }
}

#pragma mark - Application lifecycle

- (void)applicationDidFinishLaunchingWithOptions:(NSNotification *)notification {
    PUBLISH([PCEventApplicationLaunched new]);
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    PUBLISH([PCEventApplicationActive new]);
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    PUBLISH([PCEventApplicationWillResignActive new]);
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    PUBLISH([PCEventApplicationWillTerminate new]);
}

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    PUBLISH([PCEventApplicationWillEnterForeground new]);
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    PUBLISH([PCEventApplicationDidEnterBackground new]);
}

#pragma mark - Application events

#pragma mark PCEventPerformAction
SUBSCRIBE(PCEventPerformAction) {
    //
}

#pragma mark PCEventApplicationActive
SUBSCRIBE(PCEventApplicationActive) {
    PUBLISH([PCEventReportHistory new]);
    [self isEnabledToReceiveNotificationsWithCompletion:^(BOOL isEnabled) {
        PUBLISH(({
            PCEventNotificationsAuthorization *event = [PCEventNotificationsAuthorization new];
            event.notificationsAuthorization = isEnabled;
            event;
        }));
    }];
}

#pragma mark PCEventApplicationDidEnterBackground
SUBSCRIBE(PCEventApplicationDidEnterBackground) {
    PUBLISH(({
        PCEventReportHistory *reportEvent = [PCEventReportHistory new];
        reportEvent.forced = YES;
        reportEvent;
    }));
}

#pragma mark PCEventApplicationWillEnterForeground
SUBSCRIBE(PCEventApplicationWillEnterForeground) {
    [timer invalidate];
    [aidManager fetchGroupAdvertising];
    [self reportDeliveredFcmNotifications];
}

#pragma mark - PCSettingEvent

SUBSCRIBE(PCSettingEvent)
{
    if (!event.error)
    {
        [apiClient requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:YES];
    }
}

@end
