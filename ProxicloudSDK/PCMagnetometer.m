//
//  PCMagnetometer.m
//  Pods
//
//  
//
//

#import "PCMagnetometer.h"

#import "PCSettings.h"

@interface PCMagnetometer () {
    CMMotionManager *motionManager;
    
    NSOperationQueue *queue;
    
    BOOL isMonitoring;
    
    PCMagneticProximity oldProximity;
    
    double farValue;
    double nearValue;
    double immediateValue;
}
@end

@implementation PCMagnetometer

#pragma mark - PCMagnetometer

static PCMagnetometer * _sharedManager;

static dispatch_once_t once;

+ (instancetype)sharedManager {
    if (!_sharedManager) {
        //
        dispatch_once(&once, ^ {
            _sharedManager = [[self alloc] init];
        });
        //
    }
    return _sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        queue = [NSOperationQueue new];
        queue.qualityOfService = NSQualityOfServiceUserInitiated;
    }
    return self;
}

#pragma mark - 

- (void)startMonitoring {
    if (!motionManager) {
        motionManager = [[CMMotionManager alloc] init];
        motionManager.magnetometerUpdateInterval = [PCSettings sharedManager].settings.magnetometerUpdateInterval;
    }
    //
    if (!motionManager.magnetometerAvailable) {
        return;
    }
    //
    farValue = [[NSUserDefaults standardUserDefaults] doubleForKey:kPCMagnitudeFarKey];
    if (farValue<1) {
        farValue = kPCMagnitudeFar;
    }
    nearValue = [[NSUserDefaults standardUserDefaults] doubleForKey:kPCMagnitudeNearKey];
    if (nearValue<1) {
        nearValue = kPCMagnitudeNear;
    }
    immediateValue = [[NSUserDefaults standardUserDefaults] doubleForKey:kPCMagnitudeImmediateKey];
    if (immediateValue<1) {
        immediateValue = kPCMagnitudeImmediate;
    }
    //
    [motionManager setMagnetometerUpdateInterval:1/2];
    [motionManager startMagnetometerUpdatesToQueue:queue
                                       withHandler:^(CMMagnetometerData * _Nullable magnetometerData, NSError * _Nullable error) {
                                           double magnitude = sqrt (pow(magnetometerData.magneticField.x,2)+
                                                                     pow(magnetometerData.magneticField.y,2)+
                                                                     pow(magnetometerData.magneticField.z,2));
                                           //
                                           PCMagneticProximity proximity;
                                           if (magnitude>self->immediateValue) {
                                               proximity = PCMagneticProximityImmediate;
                                           } else if (magnitude>self->nearValue) {
                                               proximity = PCMagneticProximityNear;
                                           } else if (magnitude>self->farValue) {
                                               proximity = PCMagneticProximityFar;
                                           } else {
                                               proximity = PCMagneticProximityUnknown;
                                           }
                                           // Only fire an update event if the proximity has changed
                                           if (self->oldProximity!=proximity) {
                                               self->oldProximity = proximity;
                                               PUBLISH(({
                                                   PCEventMagnetometerUpdate *event = [PCEventMagnetometerUpdate new];
                                                   event.proximity = proximity;
                                                   event.rawMagnitude = magnitude;
                                                   event;
                                               }));
                                           }
                                       }];
    //
}

- (void)stopMonitoring {
    [motionManager stopMagnetometerUpdates];
}

- (CMMagnetometerData *)magnetometerData {
    return motionManager.magnetometerData;
}

- (PCMagneticProximity)magneticProximity {
    return oldProximity;
}

#pragma mark -

@end
