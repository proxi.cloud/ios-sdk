//
//  PCEnums.h
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PCTriggerType){
    // Unknown trigger
    kPCTriggerNone=0,
    /**
     *  Trigger the campaign when we enter the beacon region
     */
    kPCTriggerEnter=1,
    /**
     *  Trigger the campaign when we exit the beacon region
     */
    kPCTriggerExit=2,
    /**
     *  Trigger the campaign when we enter and/or exit the beacon region
     */
    kPCTriggerEnterExit=3
};

typedef NS_ENUM(NSInteger, PCInternalActionType) {
    /**
     *  The campaign will show the user a notification
     */
    kPCActionTypeText=1,
    /**
     *  The campaign will show the user a website
     */
    kPCActionTypeURL=2,
    /**
     *  The campaign will trigger an in-app action
     */
    kPCActionTypeInApp=3,
    /**
     *  The campaign will not be forwarded to the app, but only reported
     */
    kPCActionTypeSilent = 4,
};

/**
 * Enum for use in AppDelegate, for external developers
 */
typedef NS_ENUM(NSInteger, PCActionType) {
    /**
     *  The campaign will show the user a notification
     */
            Notification = 1,
    /**
     *  The campaign will show the user a website
     */
            Website = 2,
    /**
     *  The campaign will trigger an in-app action
     */
            DeepLink = 3,
};



/**
 PCConversionType
 Represents the conversion type for a specific campaign action
 @since 2.1.2
 */
typedef NS_ENUM(NSInteger, PCConversionType) {
    /**
     *  The campaign action can't "fire" (ex. the user has denied access to local notifications)
     */
    kPCConversionUnavailable = -2,
    /**
     *  The campaign was suppressed by the app
     */
    kPCConversionSuppressed = -1,
    /**
     *  The campaign action has been "fired" but was ignored by the user
     *
     * @discussion: The campaigns are marked as "ignored" by default. To correctly measure conversion, be sure to call [PCManager sharedManager] reportConversion: forCampaignAction:] when performing the campaign action
     */
    kPCConversionIgnored = 0,
    /**
     *  The campaign action has been performed successfully
     */
    kPCConversionSuccessful = 1
};

/**
 PCManagerAvailabilityStatus
 Represents the app’s overall iBeacon readiness, like Bluetooth being turned on,
 Background App Refresh enabled and authorization to use location services.
 @since 0.7.0
 */
typedef NS_ENUM(NSInteger, PCManagerAvailabilityStatus) {
    /**
     Background App Refresh is enabled, the app is authorized to use location services and
     Bluetooth is turned on.
     */
    PCManagerAvailabilityStatusFullyFunctional,
    
    /**
     Bluetooth is turned off. The specific status can be found in bluetoothStatus.
     */
    PCManagerAvailabilityStatusBluetoothRestricted,
    
    /**
     Wifi is turned off. The specific status can be found in wifiStatus.
     */
    PCManagerAvailabilityStatusWifiRestricted,
    /**
     This application is not enabled to use Background App Refresh. The specific status can be
     found in backgroundAppRefreshStatus.
     */
    PCManagerAvailabilityStatusBackgroundAppRefreshRestricted,
    
    /**
     This application is not authorized to use location services. The specific status can be
     found in authorizationStatus.
     */
    PCManagerAvailabilityStatusAuthorizationRestricted,
    
    /**
     This application is not connected to the Proxi Cloud Beacon Management Platform. The
     specific status can be found in connectionState.
     */
    PCManagerAvailabilityStatusConnectionRestricted,
    
    /**
     This application cannot reach the Proxi Cloud Beacon Management Platform. The specific
     status can be found in reachabilityState.
     */
    PCManagerAvailabilityStatusReachabilityRestricted,
    
    /**
     This application runs on a device that does not support iBeacon.
     @since 0.7.9
     */
    PCManagerAvailabilityStatusIBeaconUnavailable
};

/**
 PCManagerBackgroundAppRefreshStatus
 
 Represents the app’s Background App Refresh status.
 
 @since 0.7.0
 */
typedef NS_ENUM(NSInteger, PCManagerBackgroundAppRefreshStatus) {
    /**
     Background App Refresh is enabled, the app is authorized to use location services and
     Bluetooth is turned on.
     */
    PCManagerBackgroundAppRefreshStatusAvailable,
    
    /**
     This application is not enabled to use Background App Refresh. Due
     to active restrictions on Background App Refresh, the user cannot change
     this status, and may not have personally denied availability.
     
     Do not warn the user if the value of this property is set to
     PCManagerBackgroundAppRefreshStatusRestricted; a restricted user does not have
     the ability to enable multitasking for the app.
     */
    PCManagerBackgroundAppRefreshStatusRestricted,
    
    /**
     User has explicitly disabled Background App Refresh for this application, or
     Background App Refresh is disabled in Settings.
     */
    PCManagerBackgroundAppRefreshStatusDenied,
    
    /**
     This application runs on a device that does not support Background App Refresh.
     */
    PCManagerBackgroundAppRefreshStatusUnavailable
};

/**
 PCManagerAuthorizationStatus
 
 Represents the app’s authorization status for using location services.
 
 @since 0.7.0
 */

typedef NS_ENUM(NSInteger, PCLocationAuthorizationStatus) {
    /**
     User has not yet made a choice with regards to this application
     */
    PCLocationAuthorizationStatusNotDetermined,
    
    /**
     Authorization procedure has not been fully implemeneted in app.
     NSLocationAlwaysUsageDescription is missing from Info.plist.
     */
    PCLocationAuthorizationStatusUnimplemented,
    
    /**
     This application is not authorized to use location services. Due
     to active restrictions on location services, the user cannot change
     this status, and may not have personally denied authorization.
     
     Do not warn the user if the value of this property is set to
     PCManagerAuthorizationStatusRestricted; a restricted user does not have
     the ability to enable multitasking for the app.
     */
    PCLocationAuthorizationStatusRestricted,
    
    /**
     User has explicitly denied authorization for this application, or
     location services are disabled in Settings.
     */
    PCLocationAuthorizationStatusDenied,
    
    /**
     User has granted authorization to use their location at any time,
     including monitoring for regions, visits, or significant location changes.
     */
    PCLocationAuthorizationStatusAuthorized,
    
    // User has granted authorization to use their location only while
    // they are using your app.  Note: You can reflect the user's
    // continued engagement with your app using
    // -allowsBackgroundLocationUpdates.
    //
    // This value is not available on MacOS.  It should be used on iOS, tvOS and
    // watchOS.
    
    PCLocationAuthorizationStatusAuthorizedWhenInUse,
    
    /**
     This application runs on a device that does not support iBeacon.
     */
    PCLocationAuthorizationStatusUnavailable
};

typedef NS_ENUM(NSInteger, PCBluetoothStatus) {
    /**
        The Bluetooth radio is resetting (or unknown), try again later
     */
    PCBluetoothUnknown,
    /**
     The Bluetooth radio is off, unsupported or restricted
     */
    PCBluetoothOff,
    /**
     The Bluetooth radio is on, supported and accessible
     */
    PCBluetoothOn,
};

typedef NS_ENUM(NSInteger, PCWifiStatus) {
  
    PCWifiUnknown,
    
    PCWifiOn,
    
    PCWifiOff
};

typedef enum : NSUInteger {
    PCMagneticProximityUnknown = 0,
    PCMagneticProximityImmediate = 1,
    PCMagneticProximityNear = 2,
    PCMagneticProximityFar = 3,
} PCMagneticProximity;

typedef enum : NSUInteger {
    kPCMagnitudeFar = 1500,
    kPCMagnitudeNear = 2500,
    kPCMagnitudeImmediate = 3500,
} kPCMagnitudeLevels;

static NSString * const kPCMagnitudeFarKey = @"kPCMagnitudeFar";
static NSString * const kPCMagnitudeNearKey = @"kPCMagnitudeNear";
static NSString * const kPCMagnitudeImmediateKey = @"kPCMagnitudeImmediate";

typedef enum : NSUInteger {
    iBKSSettings = 0xFFF0,
    
    
} PCPeripheralService;

typedef enum : NSUInteger {
    // Full list avaialable @ https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicsHome.aspx
    iBLESystem = 0x2A23,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.system_id.xml
    iBLEModel = 0x2A24,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.model_number_string.xml
    iBLESerialNumber = 0x2A25,
    // https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.serial_number_string.xml
    iBLEFirmwareRev = 0x2A26,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.firmware_revision_string.xml
    iBLEHardwareRev = 0x2A27,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.hardware_revision_string.xml
    iBLESoftwareRev = 0x2A28,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.software_revision_string.xml
    iBLEManufacturer = 0x2A29,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.manufacturer_name_string.xml
    
    iBLEIEE = 0x2A2A,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.ieee_11073-20601_regulatory_certification_data_list.xml
    iBLEPNP = 0x2A50,
    //https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.pnp_id.xml
    
    // iBKS specific characteristic id's (see http://accent-systems.com/ibks-technical-docs/ )
    iBKSUUID  = 0xFFF1,
    iBKSMajor = 0xFFF2,
    iBKSMinor = 0xFFF3,
    iBKSCPwr  = 0xFFF4,
    iBKSAdv   = 0xFFF5,
    iBKSTxPwr = 0xFFF6,
    iBKSPwd   = 0xFFF7,
    iBKSCfg   = 0xFFF8,
    iBKSStatus= 0xFFF9,
    //
    iBKSAdvMode= 0xFFFA
    //
} PCPeripheralCharacteristic;

typedef enum : NSUInteger {
    iBKSUPC,
    iBKS105v1,
    iBKS105v2,
    FWUnknown
} PCFirmwareVersion;

@interface PCEnums : NSObject
+ (PCActionType)getActionType:(NSDictionary*)userInfo;
+ (NSString *)getActionUuid:(NSDictionary*)userInfo;

@end
