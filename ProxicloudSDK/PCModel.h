//
//  PCModel.h
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

#import <CoreBluetooth/CoreBluetooth.h>

#import <JSONModel/JSONModel.h>

#import "PCEnums.h"

#import <ObjCGeoHash/GHArea.h>

#import <ObjCGeoHash/GHRange.h>

@protocol PCModel @end
/**
 Base model (think NSObject) for all ProxicloudSDK object models
 Extends `JSONModel` so you can easily convert to and from NSDictionary, NSString or NSArray
 */
@interface PCModel : JSONModel
@end

@protocol PCMTrigger @end

/**
 Base model for all action triggers
 */
@interface PCMTrigger : PCModel
@property (strong, nonatomic) NSString *tid;
@end

@protocol PCMRegion @end
@interface PCMRegion : PCMTrigger
- (instancetype)initWithString:(NSString*)UUID;
@end

@protocol PCMWifiNetwork @end
@interface PCMWifiNetwork : PCMTrigger
- (instancetype)initWithSSID:(NSString*)SSID;
@property (strong, nonatomic) NSString *ssid;
@end

@protocol PCMAID @end
@interface PCMAID : PCMTrigger
- (instancetype)initWithAID:(NSString *)aid;
@property (strong, nonatomic) NSString *aid;
@end

/**
 Beacon action trigger
 */
@protocol PCMBeacon @end
@interface PCMBeacon : PCMTrigger
@property (strong, nonatomic) NSString *uuid;
@property (nonatomic) int major;
@property (nonatomic) int minor;

/**
 Initializer for PCMBeacon with a CLBeacon object

 @param beacon A CLBeacon object
 @return A PCMBeacon object
 */
- (instancetype)initWithCLBeacon:(CLBeacon*)beacon;

/**
 Initializer for PCMBeacon with full UUID string.

 @param fullUUID The full UUID string (hyphenated or not)
 @return Returns a PCMBeacon object. The return can also be nil if the full UUID is invalid
 */
- (instancetype)initWithString:(NSString*)fullUUID;
- (NSUUID*)UUID;
@end


/**
 Geofence action trigger
 */
@protocol PCMGeofence @end
@interface PCMGeofence : PCMTrigger

/**
 Initializer for PCMGeofence from geohash and radius

 @param geohash 14-characters length string containing geohash and radius (8 characters for the geohash and 6 characters for the radius)
 @return Returns a PCMGeofence object. The return can also be nil if the geohash is invalid
 */
- (instancetype)initWithGeoHash:(NSString *)geohash;

/**
 Initializer for the PCMGeofence

 @param region A CLCircularRegion object
 @return A PCMGeofence object
 */
- (instancetype)initWithRegion:(CLCircularRegion *)region;

@property (nonatomic) CLLocationDegrees     latitude;
@property (nonatomic) CLLocationDegrees     longitude;
@property (nonatomic) CLLocationDistance    radius;
@end

@protocol  PCMCampaignAction @end
@interface PCMCampaignAction : NSObject
@property (strong, nonatomic) NSDate            *fireDate;
@property (strong, nonatomic) NSString          *subject;
@property (strong, nonatomic) NSString          *body;
@property (strong, nonatomic) NSDictionary      *payload;
@property (strong, nonatomic) NSString          *url;
@property (strong, nonatomic) NSString          *eid; // action (notification) identifier in campaign. In Android SDK named: UUID
@property (nonatomic) PCTriggerType             trigger;
@property (nonatomic) PCInternalActionType      type;
@property (strong, nonatomic) PCMTrigger        *beacon;
@property (strong, nonatomic) NSString          *action; // unique action fire event identifier
@property (strong, nonatomic) NSDate <Optional> *expiryDate;
-(NSDateComponents *)getTriggerDate;
@end

@protocol PCMLocationAction @end
@interface PCMLocationAction : JSONModel
@property (strong, nonatomic) NSString *eid;
@property (strong, nonatomic) NSString *action;
@property (nonatomic) PCTriggerType trigger;
@property (strong, nonatomic) NSString *tid;
@end
