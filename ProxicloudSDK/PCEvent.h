//
//  PCEvent.h
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreMotion/CoreMotion.h>

#import "PCModel.h"

#pragma mark - Application life-cycle events

@protocol PCEvent @end
@interface PCEvent : NSObject
@property (strong, nonatomic) NSError *error;
@end

@interface PCEventPerformAction : PCEvent
@property (strong, nonatomic) PCMCampaignAction *campaign;
- (NSDictionary *)toDictionary;
@end

@interface PCEventPerformWhenInUseAction : PCEvent
@property (strong, nonatomic) PCMCampaignAction *campaign;
- (NSDictionary *)toDictionary;
@end

@protocol PCEventResetManager @end
@interface PCEventResetManager : PCEvent
@end

@protocol PCEventReportHistory @end
@interface PCEventReportHistory : PCEvent
@property (nonatomic) BOOL forced;
@end

@interface PCEventReportConversion : PCEvent
@property (strong, nonatomic) NSString *action;
@property (nonatomic) PCConversionType conversionType;
@property (strong, nonatomic) CLLocation *gps;
@end

@protocol PCEventUpdateHeaders @end
@interface PCEventUpdateHeaders : PCEvent
@end

@protocol PCEventStatusUpdate @end
@interface PCEventStatusUpdate : PCEvent
@end

#pragma mark - Location events

@interface PCEventRangedBeacon : PCEvent
@property (strong, nonatomic) PCMTrigger *beacon;
@property (strong, nonatomic) NSString <Optional> *pairingId;
@property (nonatomic) int rssi;
@property (nonatomic) CLProximity proximity;
@property (nonatomic) CLLocationAccuracy accuracy;
@end

@interface PCEventRegionEnter : PCEventRangedBeacon
@property (strong, nonatomic) CLLocation *location;
@end

@interface PCEventRegionExit : PCEventRangedBeacon
@property (strong, nonatomic) CLLocation *location;
@end

#pragma mark - Authorization events

@interface PCEventLocationAuthorization : PCEvent
@property (nonatomic) PCLocationAuthorizationStatus locationAuthorization;
@end

@interface PCEventBluetoothAuthorization : PCEvent
@property (nonatomic) PCBluetoothStatus bluetoothAuthorization;
@end

@interface PCEventNotificationsAuthorization : PCEvent
@property (nonatomic) BOOL notificationsAuthorization;
@end

#pragma mark - WiFi events

@interface PCEventWiFiStatus : PCEvent
@property (nonatomic) PCWifiStatus wifiStatus;
@end

/**
 Event fired when a connection is made to WiFi network
 */
@interface PCEventDeviceConnectedToWiFiNetwork : PCEvent
@property (nonatomic) PCMWifiNetwork *wifi;
@end

/**
 Event fired when a disconnection is made from WiFi network
 */
@interface PCEventDeviceDisconnectedFromWiFiNetwork : PCEvent
@property (nonatomic) PCMWifiNetwork *wifi;
@end

#pragma mark - Custom event

/**
 Custom event fired manually by app
 */
@interface PCEventCustom : PCEvent
@property (strong, nonatomic) NSString  *key;
@property (strong, nonatomic) NSString  *value;
@end


#pragma mark - AID events
/**
 Event fired when AID from campaign matches app AID

 */
@interface PCEventAID : PCEvent
@property (strong, nonatomic) PCMAID *aid;
@end

#pragma mark - CoreBluetooth events

/**
    Event fired when a software-emulated iBeacon is started
 */
@interface PCEventBluetoothEmulation : PCEvent
@end

/**
    Event fired when a new CBPeripheral has been discovered
 */
@interface PCEventDeviceDiscovered : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@end

/**
    Event fired when information about a previously discovered CBPeripheral is updated
 */
@interface PCEventDeviceUpdated : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@end

/**
    Event fired when a CBPeripheral has disconnected
 */
@interface PCEventDeviceDisconnected : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@end

/**
    Event fired when a connection is made to a CBPeripheral
 */
@interface PCEventDeviceConnected : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@end

/**
    Event fired when the list of services of a CBPeripheral has been updated
 */
@interface PCEventServicesUpdated : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@end

/**
    Event fired when a CBCharacteristic of a CBPeripheral has been updated
 */
@interface PCEventCharacteristicsUpdate : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) CBCharacteristic *characteristic;
@end

/**
    Event fired when a value has been written to a CBCharacteristic
 */
@interface PCEventCharacteristicWrite : PCEvent
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) CBCharacteristic *characteristic;
@end

#pragma mark - Magnetometer events

@interface PCEventMagnetometerUpdate : PCEvent
@property (nonatomic) PCMagneticProximity proximity;
@property (nonatomic) double rawMagnitude;
@end


#pragma mark - Application lifecycle events

@interface PCEventApplicationLaunched : PCEvent
@property (strong, nonatomic) NSDictionary *userInfo;
@end

@interface PCEventApplicationActive : PCEvent
@end

@interface PCEventApplicationForeground : PCEvent
@end

@interface PCEventApplicationWillResignActive : PCEvent
@end

@interface PCEventApplicationWillTerminate : PCEvent
@end

@interface PCEventApplicationWillEnterForeground : PCEvent
@end

@interface PCEventApplicationDidEnterBackground : PCEvent
@end

#pragma mark - Resolver events

/**
    Event fired when the general internet reachability of the PCManager changes
 */
@interface PCEventReachabilityEvent : PCEvent
@property (nonatomic) BOOL reachable;
@end


/**
 Event fired to change the back-end domain in the PCResolver.
 You should only fire this event in custom-server scenarios.
 */
@interface PCEventUpdateResolver : PCEvent
@property (strong, nonatomic) NSString *baseURL;
@property (strong, nonatomic) NSString *interactionsPath;
@property (strong, nonatomic) NSString *settingsPath;
@property (strong, nonatomic) NSString *analyticsPath;
@property (strong, nonatomic) NSString *pingPath;
@end

#pragma mark - PCSettings events

@interface PCUpdateSettingEvent : PCEvent
@property (nonatomic, strong) NSDictionary *responseDictionary;
@property (nonatomic, copy) NSString *apiKey;
@end

@interface PCSettingEvent : PCEvent
@property (nonatomic, strong) NSDictionary *settings;
@end
