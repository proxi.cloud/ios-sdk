//
//  PCMagnetometer.h
//  Pods
//
//  
//
//

#import <Foundation/Foundation.h>

#import <CoreMotion/CoreMotion.h>

#import <tolo/Tolo.h>

#import "PCEvent.h"

#import "PCEnums.h"

@interface PCMagnetometer : NSObject

+ (instancetype)sharedManager;

- (void)startMonitoring;

- (void)stopMonitoring;

- (PCMagneticProximity)magneticProximity;

@property (readonly) CMMagnetometerData *magnetometerData;

@end
