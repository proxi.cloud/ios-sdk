//
//  ProxicloudSDK.h
//  Proxi.cloud SDK
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCAIDManager.h"
#import "PCEnums.h"
#import "PCResolver.h"
#import <tolo/Tolo.h>
#import "PCEvent.h"
#import "PCHTTPRequestManager.h"
#import "PCResolver.h"

@interface PCAIDManager()
@property (nonatomic, strong) PCResolver *resolver;
@property (nonatomic, strong) PCMGetLayout *layout;
@property (nonatomic, strong) NSMutableArray<PCMAID *> *allFoundAIDs;
@end

@implementation PCAIDManager

-(instancetype)initWithResolver:(PCResolver *)resolver {
    self = [super init];
    if (self) {
        _resolver = resolver;
        _allFoundAIDs = [ NSMutableArray<PCMAID *> new];
        [self fetchGroupAdvertising];
    }
    return self;
}

-(void)fetchGroupAdvertising{
    if(isNull(self.layout)) {
        [_resolver requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:NO completion:^(PCMGetLayout * layout) {
            __weak typeof (self) weakSelf = self;
            [weakSelf checkLayout: layout];
        }];
    } else {
        [self checkLayout:_layout];
    }
}

-(void)myAIDBelongsToGroup:(PCMAID *)groupAID{
    [self isMyAIDinGroupAID:groupAID.aid useCache:NO completion:^(BOOL belongs) {
        if (belongs) {
            PUBLISH((({
                PCEventAID *event = [PCEventAID new];
                event.aid = [groupAID copy];
                event;
            })));
        }
    }];
}

-(void)checkLayout:(PCMGetLayout *)layout {
    for (PCMAction *action in layout.actions) {
        [self fetchTriggerInAction:action];
    }
    if ([self.allFoundAIDs count] > 0) {
        for (PCMAID *aid in self.allFoundAIDs) {
            [self myAIDBelongsToGroup:aid];
        }
    }
}


-(void)fetchTriggerInAction: (PCMAction *) action{
    for ( PCMTrigger *object in action.beacons) {
        if([object isKindOfClass: [PCMAID class]]) {
            PCMAID *aid = (PCMAID *) object;
            if(![self allFoundAIDsContainsAID:aid]) {
                [self.allFoundAIDs addObject:aid];
            }
        }
    }
}

-(BOOL)allFoundAIDsContainsAID:(PCMAID *)aid {
    for (PCMAID * foundAID in self.allFoundAIDs) {
        if ([foundAID.aid isEqualToString:aid.aid]) {
            return YES;
        }
    }
    return NO;
}

-(void)isMyAIDinGroupAID: (NSString *)groupAID useCache: (BOOL)useCache completion:(void(^)(BOOL))completionBlock {
    [self.resolver requestForAIDBelongingToGroup:groupAID useCache:useCache completion:^(BOOL belongs) {
        completionBlock(belongs);
    }];
}

- (void)setLayout:(PCMGetLayout *)layout {
    _layout = layout;
}

@end
