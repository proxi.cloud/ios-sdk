//
//  PCInternalModels.h
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <JSONModel/JSONModel.h>

#import "PCModel.h"

@interface PCInternalModels : PCModel
@end

#pragma mark - Resolver models

@protocol PCMContent @end
@interface PCMContent : JSONModel
@property (strong, nonatomic) NSString *subject;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSDictionary <Optional> *payload;
@property (strong, nonatomic) NSString *url;
@end

@protocol PCMTimeframe @end
@interface PCMTimeframe : JSONModel
@property (strong, nonatomic) NSDate <Optional> *start;
@property (strong, nonatomic) NSDate <Optional> *end;
@end

@protocol PCMAction @end
@interface PCMAction : JSONModel
@property (strong, nonatomic) NSString *eid;
@property (nonatomic) PCTriggerType trigger;
@property (strong, nonatomic) NSArray *beacons;
@property (nonatomic) int suppressionTime; // in seconds
@property (nonatomic) int delay; //
@property (nonatomic) BOOL reportImmediately; // when true flush the history immediately
@property (nonatomic) BOOL sendOnlyOnce; //
@property (strong, nonatomic) NSDate *deliverAt;
@property (strong, nonatomic) PCMContent *content;
@property (nonatomic) PCInternalActionType type;
@property (strong, nonatomic) NSArray <PCMTimeframe> *timeframes;
@property (strong, nonatomic) NSString *typeString DEPRECATED_ATTRIBUTE;
@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *pid;
@property (strong, nonatomic) NSDate *dt;
@end

#pragma mark - Post events

@protocol PCMMonitorEvent @end
@interface PCMMonitorEvent : JSONModel
@property (strong, nonatomic) NSString <Optional> *pid;
@property (strong, nonatomic) NSString <Optional> *location;
@property (strong, nonatomic) NSString <Optional> *wifi;
@property (strong, nonatomic) NSString <Optional> *aid;
@property (strong, nonatomic) NSDate <Optional> *dt;
@property (strong, nonatomic) NSString <Optional> *pairingId;
@property (nonatomic) int trigger;
@end

@protocol PCMWifiMonitorEvent @end
@interface PCMWifiMonitorEvent : JSONModel
@property (strong, nonatomic) NSString <Optional> *pid;
@property (strong, nonatomic) NSString <Optional> *location;
@end

@protocol PCMAIDMonitorEvent @end
@interface PCMAIDMonitorEvent : JSONModel
@property (strong, nonatomic) NSString <Optional> *pid;
@property (strong, nonatomic) NSString <Optional> *location;
@end


@protocol PCMSession @end
@interface PCMSession : JSONModel
- (instancetype)initWithId:(NSString*)uid;
@property (strong, nonatomic) NSString *pid;
@property (strong, nonatomic) NSDate *enter;
@property (nonatomic) NSTimeInterval exit;
@property (nonatomic) NSTimeInterval lastSeen;
@property (copy, nonatomic) NSString <Optional> *pairingId;
@end

#pragma mark - Post models

@protocol PCMReportAction @end
@interface PCMReportAction : JSONModel
@property (strong, nonatomic) NSString  *eid;
@property (strong, nonatomic) NSString  *uuid;
@property (strong, nonatomic) NSString  *pid;
@property (strong, nonatomic) NSDate    *dt;
@property (strong, nonatomic) NSString <Optional> *location;
@property (nonatomic) int trigger;
@property (strong, nonatomic) NSDictionary *reaction DEPRECATED_ATTRIBUTE;
@end

@protocol PCMReportConversion @end
@interface PCMReportConversion : JSONModel
@property (strong, nonatomic) NSString *action;
@property (strong, nonatomic) NSDate *dt;
@property (nonatomic) PCConversionType type;
@property (strong, nonatomic) NSString <Optional> *location;
@end

@protocol PCMPostLayout @end
@interface PCMPostLayout : JSONModel
@property (strong, nonatomic) NSDate *deviceTimestamp;
@property (strong, nonatomic) NSArray <PCMMonitorEvent> *events;
@property (strong, nonatomic) NSArray <PCMReportAction> *actions;
@property (strong, nonatomic) NSArray <PCMReportConversion> *conversions;
@end

@protocol PCMGetLayout @end
@interface PCMGetLayout : JSONModel
@property (strong, nonatomic) NSArray *accountProximityUUIDs;
@property (nonatomic) int reportTrigger;
@property (strong, nonatomic) NSArray <PCMAction> *actions;
@property (nonatomic) BOOL currentVersion;
@property (strong, nonatomic) NSArray <PCMContent> *instantActions;

- (void)checkCampaignsForBeacon:(PCMTrigger *)beacon trigger:(PCTriggerType)trigger;

@end

@interface PCMSettings : JSONModel

@property (nonatomic, assign) NSTimeInterval monitoringDelay; // in Seconds.
@property (nonatomic, assign) NSTimeInterval postSuppression; // in Seconds.
@property (nonatomic, assign) NSTimeInterval rangingSuppression; // in seconds
@property (nonatomic, readonly, copy) NSDictionary *defaultBeaconRegions;
@property (nonatomic, copy) NSDictionary *customBeaconRegions;
@property (nonatomic, assign) BOOL enableBeaconScanning;
@property (nonatomic, copy) NSString * resolverURL;
//
@property (nonatomic, assign) BOOL activeTracking;
@property (nonatomic, assign) double monitoredRadius;
@property (nonatomic, assign) double minimalAccuracy;
@property (nonatomic, assign) double magnetometerUpdateInterval;
@end

#pragma mark - JSONValueTransformer

@interface JSONValueTransformer (PCResolver)
- (NSDate *)NSDateFromNSString:(NSString*)string;
- (NSString*)JSONObjectFromNSDate:(NSDate *)date;
- (PCMTrigger *)PCMTriggerFromNSString:(NSString *)region;
- (NSString *)NSStringFromPCMTrigger:(PCMTrigger *)trigger;
@end
