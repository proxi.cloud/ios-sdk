//
//  PCResolver.m
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCResolver.h"

#import "ProxicloudSDK.h"

#import "PCInternalEvents.h"
#import "PCHTTPRequestManager.h"

#import <tolo/Tolo.h>

static NSString * const kAPIKeyPlaceholder = @"{apiKey}";
static NSString * const kGroupAIDPlaceholder = @"{groupAID}";
static NSString * const kMyAID = @"{myAID}";

static NSString * const kBaseURLKey         = @"PCSDKbaseURLPath";
static NSString * const kInteractionsKey    = @"PCSDKinteractionsPath";
static NSString * const kSettingsKey        = @"PCSDKsettingsPath";
static NSString * const kAnalyticsKey       = @"PCSDKanalyticsPath";
static NSString * const kPingKey            = @"PCSDKpingPath";
static NSString * const kUserTargetKey      = @"PCSDKUserTargetPath";
static NSString * const kAIDTargetKey       = @"PCSDKAIDTargetPath";

NSString * const PCDefaultResolverURL = @"https://cdn.proxi.cloud";
NSString * const PCDefaultInteractionsPath = @"/api/v2/sdk/gateways/{apiKey}/interactions.json";
NSString * const PCDefaultSettingsPath = @"/api/v2/sdk/gateways/{apiKey}/settings.json?platform=ios";
NSString * const PCDefaultAnalyticsPath = @"/api/v2/sdk/gateways/{apiKey}/analytics.json";
NSString * const PCDefaultPingPath = @"/api/v2/sdk/gateways/{apiKey}/active.json";
//TODO:
NSString * const PCDefaultAIDPath = @"/api/v2/sdk/gateways/{apiKey}/triggers/{groupAID}/{myAID}.json";

@interface PCResolver() {
    double timestamp;
    
    NSUserDefaults *defaults;
    
    NSString *cacheIdentifier;
    
    NSMutableDictionary *httpHeader;
    
    NSString *apiKey;
    
    NSString *baseURLString;
    
    NSString *interactionsPath;
    NSString *settingsPath;
    NSString *analyticsPath;
    NSString *pingPath;
    NSString *aidPath;
    
    NSString *targetAttributeString;
}

@end

@implementation PCResolver

- (instancetype)initWithApiKey:(NSString*)key
{
    self = [super init];
    if (self) {
        //
        apiKey = key;
        /*
         Check if we have a value in NSUserDefaults
         if yes, use those values
         if not, use the default values
         
         */
        defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults valueForKey:kBaseURLKey]) {
            baseURLString = [defaults valueForKey:kBaseURLKey];
        } else {
            baseURLString = PCDefaultResolverURL;
        }
        baseURLString = [defaults valueForKey:kBaseURLKey] ? : PCDefaultResolverURL;
        interactionsPath = [defaults valueForKey:kInteractionsKey] ? : PCDefaultInteractionsPath;
        settingsPath = [defaults valueForKey:kSettingsKey] ? : PCDefaultSettingsPath;
        analyticsPath = [defaults valueForKey:kAnalyticsKey] ? : PCDefaultAnalyticsPath;
        pingPath = [defaults valueForKey:kPingKey] ? : PCDefaultPingPath;
        aidPath = [defaults valueForKey:kAIDTargetKey] ? : PCDefaultAIDPath;
        //
        httpHeader = [NSMutableDictionary new];
        NSString *ua = [[PCUtility userAgent] toJSONString];
        [httpHeader setObject:apiKey forKey:kAPIHeaderTag];
        [httpHeader setObject:ua forKey:kUserAgentTag];
        [httpHeader setObject:@"application/json" forKey:kContentTag];

        // IDFA
        NSString *IDFA = [keychain stringForKey:kIDFA];
        if (IDFA && IDFA.length > 0)
        {
            [httpHeader setObject:IDFA forKey:kIDFA];
        }
        else
        {
            [httpHeader removeObjectForKey:kIDFA];
        }
        //
        NSString *iid = [defaults valueForKey:kPCIdentifier];
        if (isNull(iid))
        {
            iid = [[NSUUID UUID] UUIDString];
            [defaults setValue:iid forKey:kPCIdentifier];
            [defaults synchronize];
        }
        //
        cacheIdentifier = [keychain stringForKey:kCacheKey];
        if (![cacheIdentifier isEqualToString:apiKey])
        {
            [keychain setString:apiKey forKey:kCacheKey];
            [defaults synchronize];
            
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            
            PCLog(@"Cleared cache because API Key changed");
        }
        //
        [httpHeader setObject:iid forKey:kInstallId];
        //
    }
    return self;
}

- (nonnull NSURL *)interactionsURL {
    NSString *urlString = [[baseURLString stringByAppendingString:interactionsPath] stringByReplacingOccurrencesOfString:kAPIKeyPlaceholder withString:apiKey];
    if (targetAttributeString.length)
    {
        urlString = [NSString stringWithFormat:@"%@?%@",urlString, targetAttributeString];
    }
    return [NSURL URLWithString:urlString];
}

- (nonnull NSURL *)settingsURL {
    NSString *urlString = [[baseURLString stringByAppendingString:settingsPath] stringByReplacingOccurrencesOfString:kAPIKeyPlaceholder withString:apiKey];
    return [NSURL URLWithString:urlString];
}

- (nonnull NSURL *)analyticsURL {
    NSString *urlString = [[baseURLString stringByAppendingString:analyticsPath] stringByReplacingOccurrencesOfString:kAPIKeyPlaceholder withString:apiKey];
    return [NSURL URLWithString:urlString];
}

- (nonnull NSURL *)pingURL {
    NSString *urlString = [[baseURLString stringByAppendingString:pingPath] stringByReplacingOccurrencesOfString:kAPIKeyPlaceholder withString:apiKey];
    return [NSURL URLWithString:urlString];
}

- (nonnull NSURL *)aidURL:(NSString *)groupAID {
    NSString *appAdvertisingIdentifier = @"00000000-0000-0000-0000-000000000000";
    if(!isNull([keychain stringForKey:kIDFA])) {
        appAdvertisingIdentifier = [keychain stringForKey:kIDFA];
    }
    NSString *urlString = [[[
                             [baseURLString stringByAppendingString:aidPath] stringByReplacingOccurrencesOfString:kAPIKeyPlaceholder withString:apiKey]
                            stringByReplacingOccurrencesOfString:kGroupAIDPlaceholder withString:groupAID]
                           stringByReplacingOccurrencesOfString:kMyAID withString: appAdvertisingIdentifier];
    return [NSURL URLWithString:urlString];
}

- (nonnull NSMutableArray <NSURLQueryItem *> *)queryItemsWithParams:(NSDictionary * _Nullable)params
{
    NSMutableArray *queryItems = [NSMutableArray new];
    NSArray <NSString *> *sortedKeys = [params.allKeys sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *key in sortedKeys)
    {
        id value = [params valueForKey:key];
        NSString *tmpValueString = nil;
        if ([value isKindOfClass:[NSArray class]])
        {
            // check object in array and convert into string?
            tmpValueString = [(NSArray *)value componentsJoinedByString:@","];
        }
        else if ([value isKindOfClass:[NSNumber class]])
        {
            tmpValueString = [(NSNumber *)value stringValue];
        }
        else if ([value isKindOfClass:[NSString class]])
        {
            tmpValueString = [(NSString *)value copy];
        }
        else if ([value isKindOfClass:[NSDictionary class]])
        {
            // dictionary : convert to json??
            [queryItems addObjectsFromArray:[self queryItemsWithParams:(NSDictionary *)value]];
            continue;
        }
        
        NSString *itemValue = [tmpValueString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
        NSURLQueryItem *item = [NSURLQueryItem queryItemWithName:key value:itemValue];
        [queryItems addObject:item];
    }
    
    return queryItems;
}

#pragma mark - Resolver calls

- (void)ping {
    timestamp = [NSDate timeIntervalSinceReferenceDate];
    PCHTTPRequestManager *manager = [PCHTTPRequestManager sharedManager];
    NSURL *requestURL = [self pingURL];
    
    [manager getDataFromURL:requestURL headerFields:httpHeader useCache:NO completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (error)
        {
            PUBLISH(({
                PCEventPing *event = [PCEventPing new];
                event.error = [error copy];
                event;
            }));
            
            PUBLISH((({
                PCEventReachabilityEvent *event = [PCEventReachabilityEvent new];
                event.reachable = NO;
                event;
            })));
            
            return;
        }
        PUBLISH((({
            PCEventPing *event = [PCEventPing new];
            event.latency = [NSDate timeIntervalSinceReferenceDate]-self->timestamp;
            event;
        })));
        //
        PUBLISH((({
            PCEventReachabilityEvent *event = [PCEventReachabilityEvent new];
            event.reachable = YES;
            event;
        })));
    }];
    //
}

- (void)publishPCEventGetLayoutWithBeacon:(PCMBeacon*)beacon trigger:(PCTriggerType)trigger error:(NSError *)error
{
    PUBLISH(({
        PCEventGetLayout *event = [PCEventGetLayout new];
        event.error = [error copy];
        event.beacon = beacon;
        event.trigger = trigger;
        event;
    }));
}

- (void)publishPCEventGetLayoutWithAID: (NSError *)error {
    PUBLISH(({
        PCEventGetLayout *event = [PCEventGetLayout new];
        event.error = [error copy];
        event.trigger = kPCTriggerNone;
        event;
    }));
}
- (void)requestLayoutForBeacon:(PCMBeacon*)beacon trigger:(PCTriggerType)trigger useCache:(BOOL)useCache {
    [self requestLayoutForBeacon:beacon trigger:trigger useCache:useCache completion:nil];
}


- (void)requestLayoutForBeacon:(PCMBeacon*)beacon trigger:(PCTriggerType)trigger useCache:(BOOL)useCache completion:(void(^)(PCMGetLayout*))completionBlock{
    PCLog(@"❓ GET Layout %@|%@|%@",
          isNull(beacon) ? @"Without UUID" : beacon.description,
          trigger==1 ? @"Enter"  : @"Exit",
          useCache==YES ? @"Cached" : @"No cache");
    
    PCHTTPRequestManager *manager = [PCHTTPRequestManager sharedManager];
    NSURL *requestURL = [self interactionsURL];
    
    [manager getDataFromURL:requestURL headerFields:httpHeader useCache:useCache completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (error)
        {
            [self publishPCEventGetLayoutWithBeacon:beacon trigger:trigger error:error];
            return;
        }
        
        NSError *parseError = nil;
        NSDictionary * responseObject = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&parseError];
        if (parseError) {
            [self publishPCEventGetLayoutWithBeacon:beacon trigger:trigger error:parseError];
            return;
        }
        NSError *jsonError;
        //
        PCMGetLayout *layout = [[PCMGetLayout alloc] initWithDictionary:responseObject error:&jsonError];
        if(completionBlock) {
            completionBlock(layout);
        }
        //
        PUBLISH((({
            PCEventGetLayout *event = [PCEventGetLayout new];
            event.error = [jsonError copy];
            event.layout = layout;
            event;
        })));

        
        if (!isNull(beacon)) {
            [layout checkCampaignsForBeacon:beacon trigger:trigger];
        }
        
    }];
}

-(void)requestForAIDBelongingToGroup: (NSString *)groupAID useCache:(BOOL)useCache completion:(void(^)(BOOL))completionBlock {
    PCHTTPRequestManager *manager = [PCHTTPRequestManager sharedManager];
    NSURL *requestURL = [self aidURL:groupAID];
    
    [manager getDataFromURL:requestURL headerFields: httpHeader useCache:useCache completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (error) {
            [self publishPCEventGetLayoutWithAID:error];
            if (completionBlock) {
                completionBlock(NO);
            }
            return;
        }
        if (completionBlock) {
            completionBlock(YES);
        }
        return;
    }];
}

- (void)postLayout:(PCMPostLayout*)postData {
    NSError *parseError = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[postData toDictionary] options:0 error:&parseError];
    if (parseError)
    {
        PUBLISH((({
            PCEventPostLayout *event = [PCEventPostLayout new];
            event.error = [parseError copy];
            event;
        })));
        return;
    }
    
    PCHTTPRequestManager *manager = [PCHTTPRequestManager sharedManager];
    NSURL *requestURL = [self analyticsURL];
    
    [manager postData:data
                  URL:requestURL
         headerFields:httpHeader
           completion:^(NSData * _Nullable responseData, NSError * _Nullable error) {
               //
               PCEventPostLayout *postEvent = [PCEventPostLayout new];
               if (!isNull(error)) {
                   postEvent.error = [error copy];
               }
               postEvent.postData = postData;
               PUBLISH(postEvent);
    }];
}

- (void)requestSettingsWithAPIKey:(NSString *)key
{
    if (key.length == 0)
    {
        PUBLISH((({
            PCUpdateSettingEvent *event = [PCUpdateSettingEvent new];
            event.error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSURLErrorBadURL userInfo:nil];
            event;
        })));
        return;
    }

    NSURL *URL = [self settingsURL];
    
    PCHTTPRequestManager *manager = [PCHTTPRequestManager sharedManager];
    [manager getDataFromURL:URL headerFields:nil useCache:YES completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        NSError *blockError = error;
        NSDictionary *responseDict = nil;
        
        if (isNull(blockError))
        {
            NSError *parseError =nil;
            responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                           options:NSJSONReadingAllowFragments
                                                             error:&parseError];
            if (parseError)
            {
                blockError = parseError;
            }
        }
        //
        PUBLISH((({
            PCUpdateSettingEvent *event = [PCUpdateSettingEvent new];
            event.responseDictionary = responseDict;
            event.error = blockError;
            event.apiKey = key;
            event;
        })));
    }];
}

#pragma mark - Connection availability

- (BOOL)isConnected {
    return [[PCHTTPRequestManager sharedManager] isReachable];
}

#pragma mark - PCEventUpdateHeaders

SUBSCRIBE(PCEventUpdateHeaders) {
    
    NSString *IDFA = [keychain stringForKey:kIDFA];
    
    if (IDFA && IDFA.length>0) {
        [httpHeader setObject:IDFA forKey:kIDFA];
    } else {
        [httpHeader removeObjectForKey:kIDFA];
    }
}

#pragma mark - PCEventUpdateTargetAttributes

SUBSCRIBE(PCEventUpdateTargetAttributes)
{
    if (!event.targetAttributes.count)
    {
        targetAttributeString = nil;
    }
    else
    {
        NSURLComponents *urlComponents = [NSURLComponents componentsWithString:baseURLString];
        urlComponents.queryItems = [self queryItemsWithParams:event.targetAttributes];
        targetAttributeString = [urlComponents.query copy];
    }
    // report attribute as a custom event
    NSString *currentTargetAttributeString = [defaults stringForKey:kUserTargetKey];
    if (![currentTargetAttributeString isEqual:targetAttributeString]) {
        for (NSString *key in event.targetAttributes.allKeys) {
            [[PCManager sharedManager] reportEvent: key withValue: event.targetAttributes[key]];
        }
    }

    [self requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:NO];
    //
    [defaults setValue:targetAttributeString forKey:kUserTargetKey];
    [defaults synchronize];
}

#pragma mark - PCEventUpdateResolver

SUBSCRIBE(PCEventUpdateResolver) {
    BOOL hasChanged = NO;
    
    if (event.baseURL) {
        baseURLString = event.baseURL;
        [defaults setValue:baseURLString forKey:kBaseURLKey];
        hasChanged = YES;
    } else {
        baseURLString = PCDefaultResolverURL;
        [defaults removeObjectForKey:kBaseURLKey];
    }
    
    if (event.interactionsPath) {
        interactionsPath = event.interactionsPath;
        [defaults setValue:interactionsPath forKey:kInteractionsKey];
        hasChanged = YES;
    } else {
        interactionsPath = PCDefaultInteractionsPath;
        [defaults removeObjectForKey:kInteractionsKey];
    }
    
    if (event.settingsPath) {
        settingsPath = event.settingsPath;
        [defaults setValue:settingsPath forKey:kSettingsKey];
        hasChanged = YES;
    } else {
        settingsPath = PCDefaultSettingsPath;
        [defaults removeObjectForKey:kSettingsKey];
    }
    
    if (event.analyticsPath) {
        analyticsPath = event.analyticsPath;
        [defaults setValue:analyticsPath forKey:kAnalyticsKey];
        hasChanged = YES;
    } else {
        analyticsPath = PCDefaultAnalyticsPath;
        [defaults removeObjectForKey:kAnalyticsKey];
    }
    
    if (event.pingPath) {
        pingPath = event.pingPath;
        [defaults setValue:pingPath forKey:kPingKey];
        hasChanged = YES;
    } else {
        pingPath = PCDefaultPingPath;
        [defaults removeObjectForKey:kPingKey];
    }
    //
    [defaults synchronize];
    //
    if (hasChanged) {
        [self requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:NO];
    }
}

#ifdef DEBUG // only for Unittest.

- (NSString *)currentTargetAttributeString
{
    NSString *defaultTargetAttributes = [defaults valueForKey:kUserTargetKey];
    if (defaultTargetAttributes && defaultTargetAttributes.length>1) {
        targetAttributeString = defaultTargetAttributes;
    }
    return [targetAttributeString copy];
}

#endif // #ifdef DEBUG

#pragma mark - PCEventUpdateFirebaseToken

SUBSCRIBE(PCEventUpdateFirebaseToken) {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefaults valueForKey:@"kFirebaseToken"] ? : @"";
    NSString *newToken = event.token;
    if (![token isEqualToString:newToken]) {
        [userDefaults setValue:newToken forKey:@"kFirebaseToken"];
        [[PCManager sharedManager] reportEvent:@"fcm_token" withValue:newToken];
    }
}
@end
