//
//  PCInternalModels.m
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <tolo/Tolo.h>

#import "PCInternalModels.h"

#import "PCInternalEvents.h"

#import "ProxicloudSDK.h"

#import "PCUtility.h"

#import "PCEvent.h"

#import "PCSettings.h"

@implementation PCInternalModels
@end

#pragma mark - PCMSettings

@interface PCMSettings ()
@end

#pragma mark - PCMSettings

@implementation PCMSettings

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(BOOL)propertyIsIgnored:(NSString *)propertyName
{
    if ([@"defaultBeaconRegions" isEqualToString:propertyName])
    {
        return YES;
    }
    
    return NO;
}

- (instancetype)init
{
    if (self = [super init])
    {
        _monitoringDelay = 30.0f; // 30 seconds
        _rangingSuppression = 3.5f; // 3.5 seconds
        _postSuppression = 60.0f; // 60 seconds
        _enableBeaconScanning = YES;
        _defaultBeaconRegions = @{
                                  @"73676723-7400-0000-FFFF-0000FFFF0000":@"PC-0",
                                  @"73676723-7400-0000-FFFF-0000FFFF0001":@"PC-1",
                                  @"73676723-7400-0000-FFFF-0000FFFF0002":@"PC-2",
                                  @"73676723-7400-0000-FFFF-0000FFFF0003":@"PC-3",
                                  @"73676723-7400-0000-FFFF-0000FFFF0004":@"PC-4",
                                  @"73676723-7400-0000-FFFF-0000FFFF0005":@"PC-5",
                                  @"73676723-7400-0000-FFFF-0000FFFF0006":@"PC-6",
                                  @"73676723-7400-0000-FFFF-0000FFFF0007":@"PC-7",
                                  @"B9407F30-F5F8-466E-AFF9-25556B57FE6D":@"Estimote",
                                  @"F7826DA6-4FA2-4E98-8024-BC5B71E0893E":@"Kontakt.io",
                                  @"2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6":@"Radius Network",
                                  @"F0018B9B-7509-4C31-A905-1A27D39C003C":@"Beacon Inside",
                                  @"23A01AF0-232A-4518-9C0E-323FB773F5EF":@"Sensoro"
                                  };
        _resolverURL = @"https://panel.proxi.cloud";
        _activeTracking = YES;
        _monitoredRadius = 250;
        _minimalAccuracy = 500;
        _magnetometerUpdateInterval = .5f;
    }
    return self;
}

#pragma mark -

- (id)copy
{
    return [[PCMSettings alloc] initWithDictionary:[self toDictionary] error:nil];
}

@end

@implementation PCMGetLayout

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

- (BOOL)validate:(NSError *__autoreleasing *)error {
//    NSMutableArray *regions = [NSMutableArray new];
//    for (NSString *uuid in self.accountProximityUUIDs) {
//        if (uuid.length==14) {
//            PCMGeofence *fence = [[PCMGeofence alloc] initWithGeoHash:uuid];
//            if (!isNull(fence)) {
//                [regions addObject:fence];
//            }
//        } else if (uuid.length==32) {
//            PCMRegion *beacon = [[PCMRegion alloc] initWithString:uuid];
//            if (!isNull(beacon)) {
//                [regions addObject:beacon];
//            }
//        }
//    }
//    self.accountProximityUUIDs = [NSArray arrayWithArray:regions];
    return [super validate:error];
}

- (void)checkCampaignsForBeacon:(PCMTrigger *)beacon trigger:(PCTriggerType)trigger {
    
    NSDate *now = [NSDate date];
    
    for (PCMAction *action in self.actions) {
        for (PCMTrigger *actionBeacon in action.beacons) {
            if ([actionBeacon.tid isEqualToString:beacon.tid] == NO)
            {
                continue;
            }
            if ((trigger != action.trigger && action.trigger != kPCTriggerEnterExit) && trigger != kPCTriggerNone)
            {
                PCLog(@"🔕 TRIGGER %lu-%lu: %@",(unsigned long)trigger,(unsigned long)action.trigger, action.content.subject);
                continue;
            }
            
            if (action.timeframes.count && [self campaignIsInTimeframes:action.timeframes] == NO) {
                continue;
            }
            //
            if (action.sendOnlyOnce && [self campaignHasFired:action.eid]) {
                PCLog(@"🔕 Already fired: %@", action.content.subject);
                continue;
            }

            if (!isNull(action.deliverAt) && [action.deliverAt earlierDate:now]==action.deliverAt) {
                PCLog(@"🔕 Send at it's in the past: %@", action.content.subject);
                continue;
            }
            // prevent double firing!
            NSTimeInterval previousFire = [self timeIntervalSinceLastFire:action.eid];
            if (previousFire>0 && previousFire < [PCSettings sharedManager].settings.monitoringDelay) {
                PCLog(@"🔕 Suppressed: %@", action.content.subject);
                continue;
            }
            //
            if (action.suppressionTime &&
                (previousFire > 0 && previousFire < action.suppressionTime)) {
                PCLog(@"🔕 Suppressed: %@", action.content.subject);
                continue;
            }
            
            [self fireAction:action forBeacon:beacon withTrigger:trigger];
        }
    }
    //
}

#pragma mark - Helper methods

- (BOOL)campaignHasFired:(NSString*)eid {
    return !isNull([keychain stringForKey:eid]);
}

- (NSTimeInterval)timeIntervalSinceLastFire:(NSString*)eid {
    //
    NSString *lastFireString = [keychain stringForKey:eid];
    if (isNull(lastFireString)) {
        return -1;
    }
    //
    NSDate *lastFireDate = [dateFormatter dateFromString:lastFireString];
    return [[NSDate date] timeIntervalSinceDate:lastFireDate];
}

- (BOOL)campaignIsInTimeframes:(NSArray <PCMTimeframe> *)timeframes {
    
    BOOL afterStart = NO;
    BOOL beforeFinish = NO;
    
    NSDate *currentTime = [NSDate date];
    
    for (PCMTimeframe *time in timeframes) {
        
        afterStart = NO;
        beforeFinish = NO;
        
        if (isNull(time.start) || (!isNull(time.start) && [currentTime earlierDate:time.start]==time.start)) {
            PCLog(@"🔕 %@-%@",currentTime,time.start);
            afterStart = YES;
        }
        //
        if (isNull(time.end) || (!isNull(time.end) && [currentTime laterDate:time.end]==time.end)) {
            PCLog(@"🔕 %@-%@",currentTime,time.end);
            beforeFinish = YES;
        }
        //
        if (afterStart && beforeFinish) {
            return YES;
        }
    }
    return (afterStart && beforeFinish);
}

- (void)fireAction:(PCMAction *)action forBeacon:(PCMTrigger *)beacon withTrigger:(PCTriggerType)trigger
{
    PCMCampaignAction *campaignAction = [self campainActionWithAction:action beacon:beacon trigger:trigger];
    PCLog(@"🔔 Campaign \"%@\"",campaignAction.subject);
    [keychain setString:[dateFormatter stringFromDate:[NSDate date]] forKey:action.eid];
    //
    if (action.type!=kPCActionTypeSilent) {
        PUBLISH((({
            PCEventPerformAction *event = [PCEventPerformAction new];
            event.campaign = campaignAction;
            event;
        })));
    } else {
        PUBLISH((({
            PCEventInternalAction *event = [PCEventInternalAction new];
            event.campaign = campaignAction;
            event;
        })));
    }
    //
    if (action.reportImmediately) {
        PUBLISH(({
            PCEventReportHistory *reportEvent = [PCEventReportHistory new];
            reportEvent.forced = YES;
            reportEvent;
        }));
    }
}

- (PCMCampaignAction *)campainActionWithAction:(PCMAction *)action beacon:(PCMTrigger *)beacon trigger:(PCTriggerType)trigger
{
    PCMCampaignAction *campaignAction = [PCMCampaignAction new];
    campaignAction.eid = action.eid;
    campaignAction.subject = action.content.subject;
    campaignAction.body = action.content.body;
    campaignAction.payload = action.content.payload;
    campaignAction.url = action.content.url;
    campaignAction.trigger = trigger;
    campaignAction.type = action.type;
    // each time a campaign fires we generate a unique string
    // conversion measuring should use this string for reporting
    campaignAction.action = [NSUUID UUID].UUIDString;
    
    if (!isNull(action.deliverAt))
    {
        campaignAction.fireDate = action.deliverAt;
    }
    
    if (action.delay) {
        campaignAction.fireDate = [NSDate dateWithTimeIntervalSinceNow:action.delay];
        PCLog(@"🕓 Delayed %i",action.delay);
    }
    if (campaignAction.fireDate == nil) {
        campaignAction.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    }
    
    campaignAction.beacon = beacon;
    campaignAction.expiryDate = nil;
    if(action.timeframes.count > 0) {
        PCMTimeframe *lastOne = action.timeframes[0];
        campaignAction.expiryDate = lastOne.end;
    }
    return campaignAction;
}

@end

emptyImplementation(PCMMonitorEvent)
emptyImplementation(PCMWifiMonitorEvent)
emptyImplementation(PCMAIDMonitorEvent)

@implementation PCMSession

- (instancetype)initWithId:(NSString*)uid
{
    self = [super init];
    if (self) {
        NSDate *now = [NSDate date];
        _pid = uid;
        _enter = [now copy];
        _lastSeen = [now timeIntervalSince1970];
        _pairingId = [NSUUID UUID].UUIDString;
    }
    return self;
}

@end

#pragma mark - Resolver models

@implementation PCMContent

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

emptyImplementation(PCMTimeframe)

@implementation PCMAction

- (BOOL)validate:(NSError *__autoreleasing *)error {
    NSMutableArray *newBeacons = [NSMutableArray new];
    for (NSString *uuid in self.beacons) {
        if ([uuid containsString:@"ssid_"]) {
            PCMWifiNetwork *wifiNetwork = [[PCMWifiNetwork alloc] initWithSSID:uuid];
            if (!isNull(wifiNetwork)) {
                [newBeacons addObject:wifiNetwork];
            }
            continue;
        }
        if ([uuid containsString:@"aid_"]) {
            PCMAID *aid = [[PCMAID alloc] initWithAID:uuid];
            if (!isNull(aid)) {
                [newBeacons addObject:aid];
            }
            continue;
        }
        if (uuid.length==14) {
            PCMGeofence *fence = [[PCMGeofence alloc] initWithGeoHash:uuid];
            if (!isNull(fence)) {
                [newBeacons addObject:fence];
            }
        } else if (uuid.length==42) {
            PCMBeacon *beacon = [[PCMBeacon alloc] initWithString:uuid];
            if (!isNull(beacon)) {
                [newBeacons addObject:beacon];
            }
        }
    }
    self.beacons = [NSArray <PCMBeacon> arrayWithArray:newBeacons];
    return [super validate:error];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation PCMReportAction

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation PCMReportConversion

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

emptyImplementation(PCMPostLayout)


@implementation JSONValueTransformer (PCResolver)

- (NSDate *)NSDateFromNSString:(NSString*)string {
    return [dateFormatter dateFromString:string];
}

- (NSString*)JSONObjectFromNSDate:(NSDate *)date {
    return [dateFormatter stringFromDate:date];
}

- (PCMTrigger *)PCMTriggerFromNSString:(NSString *)region {
    //validating for ssid and eaid
    if ([region containsString:@"ssid_"]) {
        NSString *ssid = [region stringByReplacingOccurrencesOfString:@"ssid_" withString:@""];
        PCMWifiNetwork *trigger = [[PCMWifiNetwork alloc] initWithSSID: ssid];
        return trigger;
    }
    if ([region containsString:@"aid_"]) {
        NSString *aid = [region stringByReplacingOccurrencesOfString:@"aid_" withString:@""];
        PCMAID *trigger = [[PCMAID alloc] initWithAID: aid];
        return trigger;
    }
    if (region.length==14) {
        PCMGeofence *trigger = [[PCMGeofence alloc] initWithGeoHash:region];
        return trigger;
    } else if (region.length==32) {
        PCMRegion *trigger = [[PCMRegion alloc] initWithString:region];
        return trigger;
    } else if (region.length==42) {
        PCMBeacon *trigger = [[PCMBeacon alloc] initWithString:region];
        return trigger;
    }
    return nil;
}

- (NSString *)NSStringFromPCMTrigger:(PCMTrigger *)trigger {
    return trigger.tid;
}

@end
