//
//  PCInternalEvents.h
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCInternalModels.h"

#import "PCEvent.h"

@interface PCInternalEvents : PCEvent
@end

#pragma mark - Resolver events

@interface PCEventGetLayout : PCEvent
@property (strong, nonatomic) PCMGetLayout  *layout;
@property (strong, nonatomic) PCMBeacon     *beacon;
@property (nonatomic) PCTriggerType         trigger;
@end


@interface PCEventPostLayout : PCEvent
@property (strong, nonatomic) PCMPostLayout *postData;
@end

@interface PCEventPing : PCEvent
@property (nonatomic) double latency;
@end

@interface PCEventInternalAction : PCEventPerformAction
@end

@interface PCEventLocationUpdated : PCEvent
@property (strong, nonatomic) CLLocation *location;
@end

@interface PCEventUpdateTargetAttributes : PCEvent
@property (copy, nonatomic) NSDictionary *targetAttributes;
@end


#pragma mark - WiFi connected event

@interface PCEventDeviceConnectedViaWiFi : PCEvent
@end

@interface PCEventDeviceConnectedViaWAN : PCEvent
@end

#pragma mark - Firebase token event

@interface PCEventUpdateFirebaseToken : PCEvent
@property (copy, nonatomic) NSString *token;
@end
