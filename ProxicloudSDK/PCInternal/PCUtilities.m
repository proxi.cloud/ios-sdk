//
//  PCUtilities.m
//  Proxi.cloud SDK
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
#import <Foundation/Foundation.h>

#import "PCUtilities.h"

#import <tolo/Tolo.h>

#import "ProxicloudSDK.h"

#import "PCInternalModels.h"

#pragma mark - Constants

//String constants
NSString *const kDateFormatLocalIdentifier             = @"en_US_POSIX";
NSString *const kDateFormatString                       = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";

#pragma mark - PCUtilities

@interface PCUtilities ()

@end

@implementation PCUtilities

+ (PCMCampaignAction *)campaignActionFromDictionary:(NSDictionary *)dict {
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    PCMCampaignAction *action = [PCMCampaignAction new];
    NSDateFormatter *dateFormatter = [self defaultDateFormatter];
    
    id object = [dict objectForKey:@"fireDate"];
    if(object) {
        action.fireDate = [dateFormatter dateFromString: object];
    }
    
    object = [dict objectForKey:@"subject"];
    if (object) {
        action.subject = object;
    }
    
    object = [dict objectForKey:@"body"];
    if (object) {
        action.body = object;
    }
    
    object = [dict objectForKey:@"payload"];
    if (object) {
        action.payload = object;
    }
    
    object = [dict objectForKey:@"url"];
    if (object) {
        action.url = object;
    }
    
    object = [dict objectForKey:@"eid"];
    if (object) {
        action.eid = object;
    }
    
    object = [dict objectForKey:@"trigger"];
    if (object) {
        action.trigger = [object unsignedIntegerValue];
    }
    
    object = [dict objectForKey:@"type"];
    if (object) {
        action.type = [object unsignedIntegerValue];
    }
    
    object = [dict objectForKey:@"beacon"];
    if (object) {
        NSString *tid = [NSString stringWithFormat:@"%@",object];
        if (tid) {
            if (tid.length==14) {
                action.beacon = [[PCMGeofence alloc] initWithGeoHash:tid];
            } else if (tid.length==42) {
                action.beacon = [[PCMBeacon alloc] initWithString:tid];
            }
        
        }
    }
    
    object = [dict objectForKey:@"action"];
    if (object) {
        action.action = object;
    }
    return action;
}

+ (NSDictionary * _Nullable)dictionaryFromCampaignAction:(PCMCampaignAction * _Nonnull)action
{
    if (!action || ![action isKindOfClass:[PCMCampaignAction class]]) {
        return nil;
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    NSDateFormatter *dateFormatter = [self defaultDateFormatter];
    
    if (action.fireDate) {
        [dictionary setObject:[dateFormatter stringFromDate:action.fireDate] forKey:@"fireDate"];
    }
    
    if (action.subject) {
        [dictionary setObject:action.subject forKey:@"subject"];
    }
    
    if (action.body) {
        [dictionary setObject:action.body forKey:@"body"];
    }
    
    if (action.payload) {
        [dictionary setObject:action.payload forKey:@"payload"];
    }
    
    if (action.url) {
        [dictionary setObject:action.url forKey:@"url"];
    }
    
    if (action.eid) {
        [dictionary setObject:action.eid forKey:@"eid"];
    }
    
    if (action.trigger) {
        [dictionary setObject:@(action.trigger) forKey:@"trigger"];
    }
    
    if (action.type) {
        [dictionary setObject:@(action.type) forKey:@"type"];
    }
    
    if (action.beacon) {
        [dictionary setObject:action.beacon.tid forKey:@"beacon"];
    }
    
    if (action.action) {
        [dictionary setObject:action.action forKey:@"action"];
    }
    return dictionary;
}


+ (NSDateFormatter *)defaultDateFormatter {
    static dispatch_once_t once;
    static NSDateFormatter *dateFormatter = nil;
    
    dispatch_once(&once, ^{
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:kDateFormatLocalIdentifier]];
        [dateFormatter setDateFormat:kDateFormatString];
    });
    
    return dateFormatter;
}

@end
