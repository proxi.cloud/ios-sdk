//
//  PCLocation.m
//  Proxi.cloud SDK
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCLocation.h"

#import "NSString+PCUUID.h"

#import "PCUtility.h"

#import <tolo/Tolo.h>

#import <UIKit/UIApplication.h>

#import "ProxicloudSDK.h"

#import "PCInternalEvents.h"

#import "PCEvent.h"

#import "PCInternalModels.h"

#import "PCSettings.h"

#import "PCUtilities.h"

@import UserNotifications;
#import <UserNotifications/UserNotifications.h>

@interface PCLocation() {
    CLLocationManager *locationManager;
    //
    NSMutableDictionary *monitoredRegions;
    NSArray *rawRegions;
    //
    NSMutableDictionary *sessions;
    
    BOOL pendingLocation;
}

@end

@implementation PCLocation

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        locationManager.distanceFilter = [PCSettings sharedManager].settings.monitoredRadius;
        //
        sessions = [NSMutableDictionary new];
    }
    return self;
}

- (void)dealloc {
    [self stopMonitoring];
}

#pragma mark - External methods

- (void)requestAuthorization:(BOOL)always {
    if (![CLLocationManager locationServicesEnabled]) {
        PUBLISH(({
            PCEventLocationAuthorization *event = [PCEventLocationAuthorization new];
            event.locationAuthorization = PCLocationAuthorizationStatusUnavailable;
            event;
        }));
        return;
    }
    //
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]] && ![CLLocationManager isMonitoringAvailableForClass:[CLRegion class]]) {
        PUBLISH(({
            PCEventLocationAuthorization *event = [PCEventLocationAuthorization new];
            event.locationAuthorization = PCLocationAuthorizationStatusUnavailable;
            event;
        }));
        return;
    }
    //
    if (always) {
        [locationManager requestAlwaysAuthorization];
    } else {
        [locationManager requestWhenInUseAuthorization];
    }
    //
    if ([self authorizationStatus] == PCLocationAuthorizationStatusUnimplemented) {
        PUBLISH(({
            PCEventLocationAuthorization *event = [PCEventLocationAuthorization new];
            event.locationAuthorization = PCLocationAuthorizationStatusUnimplemented;
            event;
        }));
        //
        PCLog(@"Please set \"NSLocationAlwaysUsageDescription\" or \"NSLocationWhenInUseUsageDescription\" in info.plist of your Application!!");
    }
}

- (PCLocationAuthorizationStatus)authorizationStatus {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    PCLocationAuthorizationStatus authStatus;
    
    if (![[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] &&
        ![[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]){
        authStatus = PCLocationAuthorizationStatusUnimplemented;
        return authStatus;
    }
    //
    switch (status) {
        case kCLAuthorizationStatusRestricted:
        {
            authStatus = PCLocationAuthorizationStatusRestricted;
            break;
        }
        case kCLAuthorizationStatusDenied:
        {
            authStatus = PCLocationAuthorizationStatusDenied;
            break;
        }
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            authStatus = PCLocationAuthorizationStatusAuthorized;
            [locationManager startUpdatingLocation];
            break;
        }
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            authStatus = PCLocationAuthorizationStatusAuthorizedWhenInUse;
            [locationManager startUpdatingLocation];
            break;
        }
        case kCLAuthorizationStatusNotDetermined:
        {
            authStatus = PCLocationAuthorizationStatusNotDetermined;
            break;
        }
    }
    //
    return authStatus;
}

- (void)startMonitoring:(NSArray *)regions {
    _isMonitoring = YES;
    //
    [locationManager startUpdatingLocation];
    PCLog(@"Location updates enabled");
    //
    if (regions.count==0) {
        return;
    }
    //
    rawRegions = [NSArray arrayWithArray:regions];
    //
    if (!_gps) {
        pendingLocation = YES;
        return;
    }
    //
    [self sortAndMatchRegions];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    PUBLISH(({
        PCEventLocationAuthorization *event = [PCEventLocationAuthorization new];
        event.locationAuthorization = [self authorizationStatus];
        event;
    }));
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    PCLog(@"Entered region %@", region.identifier);
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        [locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
    } else if ([region isKindOfClass:[CLCircularRegion class]]) {
        [self updateSessionsWithGeofences:@[region]];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    PCLog(@"Exit region %@", region.identifier);
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        [self checkRegionExit];
    } else if ([region isKindOfClass:[CLCircularRegion class]]) {
        NSString *geohash = region.identifier.pathExtension;
        if (geohash.length==14) {
            PUBLISH(({
                PCEventRegionExit *exit = [PCEventRegionExit new];
                exit.beacon = [[PCMGeofence alloc] initWithGeoHash:geohash];
                exit.location = _gps;
                exit;
            }));
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region {
    if (beacons.count) {
        [self updateSessionsWithBeacons:beacons];
    }
    [self checkRegionExit];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    PCLog(@"LM Monitoring failed for %@", region.identifier);
    [self handleLocationError:error];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        [locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
        [locationManager requestStateForRegion:region];
    } else if ([region isKindOfClass:[CLCircularRegion class]]) {
        [locationManager requestStateForRegion:region];
    }
}

- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
    [self handleLocationError:error];
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {
    PCLog(@"Location events paused");
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager {
    PCLog(@"Location events resumed");
}

- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit {
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(nonnull CLRegion *)region
{
    if ([region isKindOfClass:[CLBeaconRegion class]] && state == CLRegionStateInside) {
        [locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
    } else if ([region isKindOfClass:[CLCircularRegion class]] && state == CLRegionStateInside) {
        PCLog(@"Is inside of geofence region: %@", region.identifier);
        [self locationManager:manager didEnterRegion:region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *currentLocation = [locations lastObject];
    if (!currentLocation) {
        return;
    }
    //
    if (currentLocation.horizontalAccuracy<0 || currentLocation.horizontalAccuracy>[PCSettings sharedManager].settings.minimalAccuracy) {
        return;
    }
    //
    if (!_gps || [currentLocation distanceFromLocation:_gps]>[PCSettings sharedManager].settings.monitoredRadius) {
        PUBLISH(({
            PCEventLocationUpdated *event = [PCEventLocationUpdated new];
            event.location = currentLocation;
            event;
        }));
    }
    //
    _gps = currentLocation;
    if (pendingLocation) {
        pendingLocation = NO;
        [self sortAndMatchRegions];
    }
    //
    if ([PCSettings sharedManager].settings.activeTracking==NO) {
        PCLog(@"Location events disabled");
        [locationManager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [self handleLocationError:error];
}

- (void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error {
    [self handleLocationError:error];
}

#pragma mark - Internal methods

- (void)updateSessionsWithGeofences:(NSArray *)geofences {
    for (CLCircularRegion *region in geofences) {
        NSString *regionID = region.identifier.pathExtension;
        //
        PCMSession *session = [sessions objectForKey:regionID];
        if (!session) {
            session = [[PCMSession alloc] initWithId:regionID];
        }
            if (regionID.length==14) {
                PUBLISH(({
                    PCEventRegionEnter *enter = [PCEventRegionEnter new];
                    enter.beacon = [[PCMGeofence alloc] initWithGeoHash:region.identifier.pathExtension];
                    enter.location = _gps;
                    enter.accuracy = _gps.horizontalAccuracy;
                    enter.pairingId = session.pairingId;
                    enter;
                }));
            }
        //}
        //
        session.lastSeen = [[NSDate date] timeIntervalSince1970];
        if (session.exit) {
            session.exit = 0;
        }
        //
        [sessions setObject:session forKey:regionID];
        //
    }
}

- (void)updateSessionsWithBeacons:(NSArray *)beacons {
    if (!sessions) {
        sessions = [NSMutableDictionary new];
    }
    
    for (CLBeacon *beacon in beacons) {
        PCMBeacon *sbBeacon = [[PCMBeacon alloc] initWithCLBeacon:beacon];
        
        PCMSession *session = [sessions objectForKey:sbBeacon.tid];
        if (!session) {
            session = [[PCMSession alloc] initWithId:sbBeacon.tid];
            // Because we don't have a session with this beacon, let's fire an PCEventRegionEnter event
            PUBLISH(({
                PCEventRegionEnter *enter = [PCEventRegionEnter new];
                enter.beacon = sbBeacon;
                enter.rssi = [NSNumber numberWithInteger:beacon.rssi].intValue;
                enter.proximity = beacon.proximity;
                enter.accuracy = beacon.accuracy;
                enter.location = _gps;
                enter.pairingId = session.pairingId;
                enter;
            }));
        }
        session.lastSeen = [[NSDate date] timeIntervalSince1970];
        if (session.exit) {
            session.exit = 0;
        }
        //
        [sessions setObject:session forKey:sbBeacon.tid];
        //
        if (beacon.proximity!=CLProximityUnknown) {
            PUBLISH(({
                PCEventRangedBeacon *event = [PCEventRangedBeacon new];
                event.beacon = sbBeacon;
                event.rssi = [NSNumber numberWithInteger:beacon.rssi].intValue;
                event.proximity = beacon.proximity;
                event.accuracy = beacon.accuracy;
                event.pairingId = session.pairingId;
                event;
            }));
        }
    }
}

- (void)checkRegionExit {
    //
    NSTimeInterval monitoringDelay = [PCSettings sharedManager].settings.monitoringDelay;
    NSTimeInterval rangingDelay = [PCSettings sharedManager].settings.rangingSuppression;
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    for (PCMSession *session in sessions.allValues) {
        if (session.lastSeen + monitoringDelay <= now ) {
            if (session.exit<=0) {
                PCLog(@"Setting exit for %@", session.pid);
                session.exit = now;
            } else if ( session.exit + rangingDelay <= now ) {
                PUBLISH(({
                    PCEventRegionExit *exit = [PCEventRegionExit new];
                    if (session.pid.length==14) {
                        exit.beacon = [[PCMGeofence alloc] initWithGeoHash:session.pid];
                    } else if (session.pid.length==42) {
                        exit.beacon = [[PCMBeacon alloc] initWithString:session.pid];
                    }
                    exit.location = _gps;
                    exit.pairingId = session.pairingId;
                    exit;
                }));
            }
        }
    }
}

- (void)startMonitoringForBeaconRegion:(PCMTrigger *)region {
    NSUUID *uuid;
    CLBeaconRegion *beaconRegion;
    //
    if ([region isKindOfClass:[PCMRegion class]]) {
        uuid = [[NSUUID alloc] initWithUUIDString:[NSString hyphenateUUIDString:region.tid]];
        beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:[kPCIdentifier stringByAppendingPathExtension:region.tid]];
    } else if ([region isKindOfClass:[PCMBeacon class]]) {
        uuid = [[NSUUID alloc] initWithUUIDString:[NSString hyphenateUUIDString:[(PCMBeacon*)region uuid]]];
        beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                               major:[(PCMBeacon*)region major]
                                                               minor:[(PCMBeacon*)region minor]
                                                          identifier:[kPCIdentifier stringByAppendingPathExtension:region.tid]];
    }
    [locationManager startMonitoringForRegion:beaconRegion];
}

- (void)startMonitoringForGeoRegion:(PCMGeofence *)region {
    CLCircularRegion *circularRegion = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(region.latitude, region.longitude) radius:region.radius identifier:[kPCIdentifier stringByAppendingPathExtension:region.tid]];
    [locationManager startMonitoringForRegion:circularRegion];
}

- (void)stopMonitoring {
    for (CLRegion *region in locationManager.monitoredRegions.allObjects) {
        if ([region.identifier rangeOfString:kPCIdentifier].location!=NSNotFound) {
            [locationManager stopMonitoringForRegion:region];
            PCLog(@"Stopped monitoring for %@",region.identifier);
        }
    }
}

- (void)handleLocationError:(NSError *)error {
    if (isNull(error)) {
        return;
    }
    PCLog(@"Location error: %@", error);
    switch (error.code) {
        case kCLErrorDenied: {
            // user denied!
            break;
        }
        case kCLErrorRangingUnavailable: {
            // airplane mode, location or bluetooth unavailable!
            break;
        }
        case kCLErrorRegionMonitoringDenied: {
            // user denied access to region monitoring!
            break;
        }
        case kCLErrorRegionMonitoringSetupDelayed: {
            // region monitoring was delayed
            break;
        }
        case kCLErrorRegionMonitoringFailure: {
            // failed to start monitoring for a region (too many monitored regions or radius of geofence is too high
            break;
        }
        case kCLErrorRangingFailure: {
            // general ranging error
            break;
        }
        case kCLErrorRegionMonitoringResponseDelayed: {
            PCLog(@"Alternate region: %@",kCLErrorUserInfoAlternateRegionKey);
            break;
        }
        default:
            break;
    }
}

- (void)addLocationNotificationsIn:(NSArray*) regions withActions: (NSArray*) actions {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // remove old location notifications
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            NSMutableSet <PCMLocationAction> *expectedNotifications = [NSMutableSet <PCMLocationAction> new];
            NSArray *keyedActions = [[NSUserDefaults standardUserDefaults] objectForKey:@"addedNotifications"];
            NSSet *actionEventSet = [NSSet setWithArray:keyedActions];
            
            //expectedNotifications array will contain all notifications previously saved in UserDefaults
            for (NSString *json in actionEventSet) {
                NSError *error;
                PCMLocationAction *action = [[PCMLocationAction alloc] initWithString:json error:&error];
                if (!error && !isNull(action)) {
                    [expectedNotifications addObject:action];
                }
            }
            // because some code outside ProxiCloud SDK can remove pending notifications
            NSInteger proxiNotificationsCounter = 0;
                for (UNNotificationRequest *request in requests){
                    if (request.content.userInfo[@"locationNotification"] && request.content.userInfo[@"action"]){
                        proxiNotificationsCounter++;
                        for (PCMLocationAction *expected in expectedNotifications){
                            if ([expected.action isEqualToString:request.content.userInfo[@"action"]]){
                                [expectedNotifications removeObject:expected];
                                break;
                            }
                        }
                    }
                    [center removePendingNotificationRequestsWithIdentifiers:[NSArray arrayWithObjects: request.identifier, nil]];
                }
            if (proxiNotificationsCounter > 0) {
                for (PCMLocationAction *action in expectedNotifications){
                    PCMCampaignAction *campaign = [PCMCampaignAction new];
                    campaign.eid = action.eid;
                    campaign.action = action.action;
                    campaign.trigger = action.trigger;
                    campaign.beacon = [PCMTrigger new];
                    campaign.beacon.tid = action.tid;

                    PUBLISH((({
                        PCEventPerformWhenInUseAction *event = [PCEventPerformWhenInUseAction new];
                        event.campaign = campaign;
                        event;
                    })));
                    [[PCManager sharedManager] reportConversion:kPCConversionIgnored forCampaignAction:action.action];
                }
            }
        
            
            NSMutableSet *geofences = [NSMutableSet new];
            NSMutableSet *actionsWithGeofences = [NSMutableSet new];
            for (NSString *region in regions) {
                if (region.length==14 && ![region containsString:@"ssid_"]) {
                    PCMGeofence *fence = [[PCMGeofence alloc] initWithGeoHash:region];
                    if (!isNull(fence)) {
                        [geofences addObject:fence];
                    }
                }
            }
            NSMutableArray *sortedGeofences = [NSMutableArray arrayWithArray:[self sortGeolocations:geofences.allObjects]];
            for (PCMAction *action in actions){
                // remove silent actions
                if (action.type == 4) {
                    continue;
                }
                for (PCMTrigger *trigger in action.beacons) {
                    if ([trigger isKindOfClass:[PCMGeofence class]]) {
                        [actionsWithGeofences addObject:action];
                        break;
                    }
                }
            }
            NSInteger addedCounter = 0;
            NSMutableSet <PCMLocationAction> *addedNotifications = [NSMutableSet <PCMLocationAction> new];
            for (PCMGeofence *geofence in sortedGeofences){
                for (PCMAction *action in actionsWithGeofences){
                    for (PCMTrigger *trigger in action.beacons) {
                        if ([trigger isKindOfClass:[PCMGeofence class]]) {
                            PCMGeofence *fence = (PCMGeofence *)trigger;
                            if (fence.latitude == geofence.latitude && fence.longitude == geofence.longitude && fence.radius == geofence.radius){
                                for (PCMCampaignAction *campaignAction in [self campainActionsWithAction:action beacon:fence triggerType:action.trigger]){
                                    [self addLocationNotificationForCampaign:campaignAction withAttachmentUrl:nil inGeofence:fence];
                                    addedCounter += 1;
                                    [addedNotifications addObject: [self locationActionFromCampaignAction:campaignAction]];

                                    if (addedCounter == kPCMaxGeonotificationCount){
                                        NSMutableSet *keyedAddedActions = [NSMutableSet new];
                                        NSLock *mutableSetLock = [[NSLock alloc] init];
                                        [addedNotifications.allObjects enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                            NSString *jsonString = [obj toJSONString];
                                            [mutableSetLock lock];
                                            [keyedAddedActions addObject:jsonString];
                                            [mutableSetLock unlock];
                                        }];
                                        [[NSUserDefaults standardUserDefaults] setObject:keyedAddedActions.allObjects forKey:@"addedNotifications"];
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            NSMutableSet *keyedAddedActions = [NSMutableSet new];
            NSLock *mutableSetLock = [[NSLock alloc] init];
            [addedNotifications.allObjects enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *jsonString = [obj toJSONString];
                [mutableSetLock lock];
                [keyedAddedActions addObject:jsonString];
                [mutableSetLock unlock];
            }];
            [[NSUserDefaults standardUserDefaults] setObject:keyedAddedActions.allObjects forKey:@"addedNotifications"];
        }];
    });
}

- (void)addLocationNotificationForCampaign:(PCMCampaignAction *)campaignAction withAttachmentUrl: (NSURL *) attachmentUrl inGeofence: (PCMGeofence *) geofence {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
     content.title = [NSString stringWithFormat:@"%@", campaignAction.subject];
     content.body = [NSString stringWithFormat:@"%@", campaignAction.body];
     content.sound = [UNNotificationSound defaultSound];
     //NSString *campaignAction = [NSUUID UUID].UUIDString;
    content.userInfo = @{@"action_type": [NSString stringWithFormat:@"%i", campaignAction.type], @"url": campaignAction.url,  @"action": campaignAction.action, @"locationNotification": @"true", @"eid": campaignAction.eid, @"trigger": [NSString stringWithFormat:@"%i", campaignAction.trigger], @"tid" : campaignAction.beacon.tid};
     /*if (attachmentUrl) {
         NSError *attachError = nil;
         UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"img" URL:attachmentUrl options:nil error:&attachError];
         if (attachment) {
             content.attachments = @[attachment];
         }
     }*/
    // UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:campaign.getTriggerDate repeats:NO];
     CLCircularRegion *region = [[CLCircularRegion new] initWithCenter:CLLocationCoordinate2DMake(geofence.latitude, geofence.longitude) radius:geofence.radius identifier: campaignAction.action];
    BOOL notifyOnEntry = campaignAction.trigger == 1;
    BOOL notifyOnExit = campaignAction.trigger == 2;
    region.notifyOnEntry = notifyOnEntry;
     region.notifyOnExit = notifyOnExit;
     UNLocationNotificationTrigger * trigger = [UNLocationNotificationTrigger triggerWithRegion: region repeats:false];
     UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:campaignAction.action content:content trigger:trigger];
     [center addNotificationRequest:request withCompletionHandler:nil];
//    [[PCManager sharedManager] isEnabledToReceiveNotificationsWithCompletion:^(BOOL isEnabled) {
//        __weak typeof(self) weakSelf = self;
//        [weakSelf reportConversion:isEnabled withCampaignAction:campaignAction.action];
//    }];
}

- (NSArray *)campainActionsWithAction:(PCMAction *)action beacon:(PCMTrigger *)beacon triggerType:(PCTriggerType)triggerType
{
    NSMutableArray *campaignActions = [NSMutableArray new];

    if (triggerType == kPCTriggerEnter || triggerType == kPCTriggerEnterExit ) {
        PCMCampaignAction *campaignAction = [PCMCampaignAction new];
        campaignAction.eid = action.eid;
        campaignAction.subject = action.content.subject;
        campaignAction.body = action.content.body;
        campaignAction.payload = action.content.payload;
        campaignAction.url = action.content.url;
        campaignAction.trigger = kPCTriggerEnter;
        campaignAction.type = action.type;
        campaignAction.action = [NSUUID UUID].UUIDString;
        if (!isNull(action.deliverAt))
        {
            campaignAction.fireDate = action.deliverAt;
        }
        campaignAction.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        campaignAction.beacon = beacon;
        campaignAction.expiryDate = nil;
        if(action.timeframes.count > 0) {
            PCMTimeframe *lastOne = action.timeframes[0];
            campaignAction.expiryDate = lastOne.end;
        }
        [campaignActions addObject:campaignAction];
    }
    if (triggerType == kPCTriggerExit || triggerType == kPCTriggerEnterExit ) {
        PCMCampaignAction *campaignAction = [PCMCampaignAction new];
        campaignAction.eid = action.eid;
        campaignAction.subject = action.content.subject;
        campaignAction.body = action.content.body;
        campaignAction.payload = action.content.payload;
        campaignAction.url = action.content.url;
        campaignAction.trigger = kPCTriggerExit;
        campaignAction.type = action.type;
        campaignAction.action = [NSUUID UUID].UUIDString;
        if (!isNull(action.deliverAt))
        {
            campaignAction.fireDate = action.deliverAt;
        }
        campaignAction.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        campaignAction.beacon = beacon;
        campaignAction.expiryDate = nil;
        if(action.timeframes.count > 0) {
            PCMTimeframe *lastOne = action.timeframes[0];
            campaignAction.expiryDate = lastOne.end;
        }
        [campaignActions addObject:campaignAction];
    }
    return campaignActions;
}

- (PCMLocationAction *)locationActionFromCampaignAction:(PCMCampaignAction *)campaignAction
{
    PCMLocationAction *locationAction = [PCMLocationAction new];
    locationAction.eid = campaignAction.eid;
    locationAction.action = campaignAction.action;
    locationAction.trigger = campaignAction.trigger;
    locationAction.tid = campaignAction.beacon.tid;
    
    return locationAction;
}

- (void)sortAndMatchRegions {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSMutableSet *triggers = [NSMutableSet new];
    NSMutableSet *beaconRegions = [NSMutableSet new];
    NSMutableSet *geofences = [NSMutableSet new];
    NSMutableSet *beacons = [NSMutableSet new];
    //
    
    //check if id is wifi or target group
    for (NSString *region in self->rawRegions) {
        if (region.length==14 && ![region containsString:@"ssid_"]) {
            PCMGeofence *fence = [[PCMGeofence alloc] initWithGeoHash:region];
            if (!isNull(fence)) {
                [triggers addObject:fence];
                [geofences addObject:fence];
            }
        } else if (region.length==32 && ![region containsString:@"ssid_"]) {
            PCMRegion *beacon = [[PCMRegion alloc] initWithString:region];
            if (!isNull(beacon)) {
                [triggers addObject:beacon];
                [beaconRegions addObject:beacon];
            }
        } else if (region.length==42 && ![region containsString:@"ssid_"]) {
            PCMBeacon *beacon = [[PCMBeacon alloc] initWithString:region];
            if (!isNull(beacon)) {
                [triggers addObject:beacon];
                [beacons addObject:beacon];
            }
        }
    }
    //
    if (triggers.count < kPCMaxMonitoringRegionCount) {
        // if we have less that 20 regions, we don't need to do any tricks
        self->monitoredRegions = [NSMutableDictionary new];
        for (PCMTrigger *t in triggers) {
            if (t.tid) {
                [self->monitoredRegions setObject:t forKey:t.tid];
            }
        }
    } else {
        // we add the beacon regions first
        self->monitoredRegions = [NSMutableDictionary new];
        for (PCMTrigger *t in beaconRegions) {
            [self->monitoredRegions setObject:t forKey:t.tid];
        }
        // we sort geofences by distance from us
        NSMutableArray *locations = [NSMutableArray arrayWithArray:[self sortGeolocations:geofences.allObjects]];
        // and build our monitoredRegions
        while (self->monitoredRegions.allKeys.count<kPCMaxMonitoringRegionCount && locations.count>0) {
            PCMTrigger *trigger = [locations firstObject];
            if (trigger) {
                [locations removeObject:trigger];
                [self->monitoredRegions setObject:trigger forKey:trigger.tid];
            }
        }
    }
    // we remove already monitored regions
    NSArray *regionIDs = [NSArray arrayWithArray:self->locationManager.monitoredRegions.allObjects];
    for (CLRegion *region in regionIDs) {
        NSString *rid = region.identifier.pathExtension;
        if (rid.length && !isNull([self->monitoredRegions valueForKey:rid])) {
            [self->monitoredRegions removeObjectForKey:rid];
            if ([region isKindOfClass:[CLBeaconRegion class]]) {
                [self->locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
            } else if ([region isKindOfClass:[CLCircularRegion class]]) {
                [self->locationManager requestStateForRegion:region];
            }
        } else {
            PCLog(@"We'll stop monitoring for %@", rid);
            [self->locationManager stopMonitoringForRegion:region];
        }
    }

    PCLog(@"We'll start monitoring for %@", self->monitoredRegions.allKeys);
    // we start monitoring for the remaining regions
    for (PCMTrigger *trigger in self->monitoredRegions.allValues) {
        if ([trigger isKindOfClass:[PCMGeofence class]]) {
            [self startMonitoringForGeoRegion:(PCMGeofence *)trigger];
        } else if ([trigger isKindOfClass:[PCMRegion class]] || ([trigger isKindOfClass:[PCMBeacon class]])) {
            [self startMonitoringForBeaconRegion:trigger];
        }
    }
    });
}

- (NSArray *)sortGeolocations:(NSArray *)locations {
    CLLocation *currentLocation = _gps;
    //
    NSArray *sorted = [locations sortedArrayUsingComparator:^NSComparisonResult(PCMGeofence *location1, PCMGeofence *location2) {
        CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:location1.latitude longitude:location1.longitude];
        
        CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:location2.latitude longitude:location2.longitude];
        
        if ([currentLocation distanceFromLocation:loc1] < [currentLocation distanceFromLocation:loc2]) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
    }];
    
    return sorted;
}

#pragma mark - Events
#pragma mark PCEventApplicationWillEnterForeground
SUBSCRIBE(PCEventApplicationWillEnterForeground) {
    if ([PCManager sharedManager].locationAuthorization==PCLocationAuthorizationStatusAuthorized) {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
            [locationManager stopMonitoringSignificantLocationChanges];
            PCLog(@"Significant location events disabled");
        }
    }
}

#pragma mark PCEventApplicationDidEnterBackground
SUBSCRIBE(PCEventApplicationDidEnterBackground) {
    if ([PCManager sharedManager].locationAuthorization==PCLocationAuthorizationStatusAuthorized) {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
            [locationManager startMonitoringSignificantLocationChanges];
            PCLog(@"Significant location events enabled");
        }
    }
}

SUBSCRIBE(PCEventRegionExit) {
    if (event.beacon.tid.length)
    {
        [sessions removeObjectForKey:event.beacon.tid];
        PCLog(@"Session closed for %@", event.beacon.tid);
    }
    else
    {
        PCLog(@"WARNING Tried to close Unknown Session for %@", event.beacon);
    }
    
}

#pragma mark - For Unit Tests

- (NSDictionary *)currentSessions {
    return [sessions copy];
}

- (void)addNotificationForCampaign:(PCMCampaignAction *)campaign withAttachmentUrl: (NSURL *) attachmentUrl {
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = [NSString stringWithFormat:@"%@", campaign.subject];
    content.body = [NSString stringWithFormat:@"%@", campaign.body];
    content.sound = [UNNotificationSound defaultSound];
    content.userInfo = @{@"action_type": [NSString stringWithFormat:@"%i", campaign.type], @"url": campaign.url,  @"action": campaign.action};
    if (attachmentUrl) {
        NSError *attachError = nil;
        UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"img" URL:attachmentUrl options:nil error:&attachError];
        if (attachment) {
            content.attachments = @[attachment];
        }
    }
    NSString *identifier = campaign.eid;
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:campaign.getTriggerDate repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:nil];
    [[PCManager sharedManager] isEnabledToReceiveNotificationsWithCompletion:^(BOOL isEnabled) {
        __weak typeof(self) weakSelf = self;
        [weakSelf reportConversion:isEnabled withCampaignAction:campaign.action];
    }];
}

#pragma mark - Notifications
    SUBSCRIBE(PCEventPerformAction) {
            NSURLSessionDownloadTask *downloadTask;
            NSURL *attachmentURL = [NSURL URLWithString: [event.campaign.payload valueForKey:@"image"] ];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            downloadTask = [session downloadTaskWithURL:attachmentURL completionHandler:^(NSURL *fileLocation, NSURLResponse *response, NSError *error) {
                if (error != nil) {
                    [self addNotificationForCampaign:event.campaign withAttachmentUrl:nil];
                    return;
                }
                else {
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    NSString *fileSuffix = attachmentURL.lastPathComponent;
                    NSURL *typedAttachmentURL = [NSURL fileURLWithPath:[(NSString *_Nonnull)fileLocation.path stringByAppendingString:fileSuffix]];
                    [fileManager moveItemAtURL:fileLocation toURL:typedAttachmentURL error:&error];
                    [self addNotificationForCampaign:event.campaign withAttachmentUrl:typedAttachmentURL];

                }
            }];
            [downloadTask resume];
    }

- (void)reportConversion: (BOOL)successful withCampaignAction:(NSString *)action {
    if(successful) {
        [[PCManager sharedManager] reportConversion:kPCConversionIgnored forCampaignAction:action];
    } else {
        [[PCManager sharedManager] reportConversion:kPCConversionUnavailable forCampaignAction:action];
    }
}

@end
