//
//  PCAnalytics.m
//  Proxi.cloud SDK 
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
#import <tolo/Tolo.h>

#import <ObjCGeoHash/GeoHash.h>

#import "ProxicloudSDK.h"

#import "PCInternalEvents.h"

#import "PCAnalytics.h"

#import "PCResolver.h"

#import "PCSettings.h"

#pragma mark - Constants

NSString * const kPCEvents = @"events";
NSString * const kPCActions = @"actions";
NSString * const kPCConversions = @"conversions";

@interface PCAnalytics () {
    NSUserDefaults *defaults;
    //
    NSMutableSet <PCMMonitorEvent> *events;
    //
    NSMutableSet <PCMReportAction> *actions;
    
    NSMutableSet <PCMReportConversion> *conversions;
    
    CLLocation *currentLocation;
}

@end

@implementation PCAnalytics

- (instancetype)init
{
    self = [super init];
    if (self) {
        defaults = [[NSUserDefaults alloc] initWithSuiteName:kPCIdentifier];
        //
        NSArray *keyedEvents = [defaults objectForKey:kPCEvents];
        NSSet *monitorEventSet = [NSSet setWithArray:keyedEvents];
        events = [NSMutableSet <PCMMonitorEvent> new];
        for (NSString *json in monitorEventSet) {
            NSError *error;
            PCMMonitorEvent *event = [[PCMMonitorEvent alloc] initWithString:json error:&error];
            if (!error && !isNull(event)) {
                [events addObject:event];
            }
        }
        //
        NSArray *keyedActions = [defaults objectForKey:kPCActions];
        NSSet *actionEventSet = [NSSet setWithArray:keyedActions];
        actions = [NSMutableSet <PCMReportAction> new];
        for (NSString *json in actionEventSet) {
            NSError *error;
            PCMReportAction *action = [[PCMReportAction alloc] initWithString:json error:&error];
            if (!error && !isNull(action)) {
                [actions addObject:action];
            }
        }
        
        NSArray *keyedConversions = [defaults objectForKey:kPCConversions];
        NSSet *conversionEventSet = [NSSet setWithArray:keyedConversions];
        conversions = [NSMutableSet <PCMReportConversion> new];
        for (NSString *json in conversionEventSet) {
            NSError *error;
            PCMReportConversion *conversion = [[PCMReportConversion alloc] initWithString:json error:&error];
            if (!error && !isNull(conversion)) {
                [conversions addObject:conversion];
            }
        }
    }
    return self;
}

- (NSArray <PCMMonitorEvent> *)events {
    return (NSArray <PCMMonitorEvent> *)[NSArray arrayWithArray:events.allObjects];
}

- (NSArray <PCMReportAction> *)actions {
    return (NSArray <PCMReportAction> *)[NSArray arrayWithArray:actions.allObjects];
}

- (NSArray <PCMReportConversion> *)conversions {
    return (NSArray <PCMReportConversion> *)[NSArray arrayWithArray:conversions.allObjects];
}



#pragma mark - Location events

SUBSCRIBE(PCEventRegionEnter) {
    //
    PCMMonitorEvent *enter = [PCMMonitorEvent new];
    enter.pid = event.beacon.tid;
    enter.dt = [NSDate date];
    enter.trigger = 1;
    enter.location = [GeoHash hashForLatitude:event.location.coordinate.latitude longitude:event.location.coordinate.longitude length:9];
    enter.pairingId = event.pairingId;
    //
    [events addObject:enter];
    //
    [self updateHistory];
}

SUBSCRIBE(PCEventRegionExit) {
    //
    PCMMonitorEvent *exit = [PCMMonitorEvent new];
    exit.pid = event.beacon.tid;
    exit.dt = [NSDate date];
    exit.trigger = 2;
    exit.location = [GeoHash hashForLatitude:event.location.coordinate.latitude longitude:event.location.coordinate.longitude length:9];
    exit.pairingId = event.pairingId;
    //
    [events addObject:exit];
    //
    [self updateHistory];
}

#pragma mark - Wifi events

SUBSCRIBE(PCEventDeviceConnectedToWiFiNetwork) {
    PCMMonitorEvent *enterWifi = [PCMMonitorEvent new];
    enterWifi.pid = event.wifi.tid;
    enterWifi.dt = [NSDate date];
    enterWifi.trigger = 1;
    enterWifi.location = currentLocation ?  [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9] : nil;
    enterWifi.wifi = event.wifi.ssid;
    [events addObject: enterWifi];
    [self updateHistory];
}

SUBSCRIBE(PCEventDeviceDisconnectedFromWiFiNetwork) {
    PCMMonitorEvent *exitWifi = [PCMMonitorEvent new];
    exitWifi.pid = event.wifi.tid;
    exitWifi.dt = [NSDate date];
    exitWifi.trigger = 2;
    exitWifi.location = currentLocation ? [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9] : nil;
    exitWifi.wifi = event.wifi.ssid;
    [events addObject: exitWifi];
    [self updateHistory];
}

SUBSCRIBE(PCEventCustom) {
    PCMMonitorEvent *customEvent = [PCMMonitorEvent new];
    customEvent.dt = [NSDate date];
    customEvent.trigger = 1;
    customEvent.location = currentLocation ? [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9] : nil;
    customEvent.pid  = event.key;
    customEvent.pairingId = event.value;
    [events addObject: customEvent];
    [self updateHistory];
}



SUBSCRIBE(PCEventPerformAction) {
    PCMReportAction *report = [PCMReportAction new];
    report.eid = event.campaign.eid;
    report.uuid = event.campaign.action;
    if (event.campaign.fireDate) {
        report.dt = event.campaign.fireDate;
    } else {
        report.dt = [NSDate date];
    }
    report.trigger = event.campaign.trigger;
    report.pid = event.campaign.beacon.tid;
    if (currentLocation) {
        report.location = [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9];
    }
    //
    [actions addObject:report];
    //
    [self updateHistory];
    //
}

SUBSCRIBE(PCEventPerformWhenInUseAction) {
    PCMReportAction *report = [PCMReportAction new];
    report.eid = event.campaign.eid;
    report.uuid = event.campaign.action;
    if (event.campaign.fireDate) {
        report.dt = event.campaign.fireDate;
    } else {
        report.dt = [NSDate date];
    }
    report.trigger = event.campaign.trigger;
    report.pid = event.campaign.beacon.tid;
    if (currentLocation) {
        report.location = [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9];
    }
    //
    [actions addObject:report];
    //
    [self updateHistory];
    //
}

SUBSCRIBE(PCEventInternalAction) {
    PCMReportAction *report = [PCMReportAction new];
    report.eid = event.campaign.eid;
    report.uuid = event.campaign.action;
    if (event.campaign.fireDate) {
        report.dt = event.campaign.fireDate;
    } else {
        report.dt = [NSDate date];
    }
    report.trigger = event.campaign.trigger;
    report.pid = event.campaign.beacon.tid;
    if (currentLocation) {
        report.location = [GeoHash hashForLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude length:9];
    }
    //
    [actions addObject:report];
    //
    [self updateHistory];
    //
}

SUBSCRIBE(PCEventReportConversion) {
    if (event.error) {
        return;
    }
    PCMReportConversion *conversion = [PCMReportConversion new];
    conversion.dt = [NSDate date];
    conversion.action = event.action;
    conversion.type = event.conversionType;
    conversion.location = [GeoHash hashForLatitude:event.gps.coordinate.latitude longitude:event.gps.coordinate.longitude length:9];
    //
    [conversions addObject:conversion];
    //
    [self updateHistory];
}

SUBSCRIBE(PCEventLocationUpdated) {
    if (event.error) {
        return;
    }
    currentLocation = event.location;
}

#pragma mark - Resolver events

SUBSCRIBE(PCEventPostLayout) {
    if (isNull(event.error)) {
        for (PCMMonitorEvent *monitor in event.postData.events)
        {
            [events removeObject:monitor];
        }
        
        for (PCMReportAction *action in event.postData.actions)
        {
            [actions removeObject:action];
        }
        
        for (PCMReportConversion *conversion in event.postData.conversions)
        {
            [conversions removeObject:conversion];
        }
        //
        [self updateHistory];
    }
}

- (void)updateHistory {
    NSMutableSet *keyedEvents = [NSMutableSet new];
    NSLock *mutableSetLock = [[NSLock alloc] init];
    [events.allObjects enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *jsonString = [obj toJSONString];
        [mutableSetLock lock];
        [keyedEvents addObject:jsonString];
        [mutableSetLock unlock];
    }];
    //
    NSMutableSet *keyedActions = [NSMutableSet new];
    [actions.allObjects enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *jsonString = [obj toJSONString];
        [mutableSetLock lock];
        [keyedActions addObject:jsonString];
        [mutableSetLock unlock];
    }];
    //
    NSMutableSet *keyedConversions = [NSMutableSet new];
    [conversions.allObjects enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *jsonString = [obj toJSONString];
        [mutableSetLock lock];
        [keyedConversions addObject:jsonString];
        [mutableSetLock unlock];
    }];
    //
    [defaults setObject:keyedEvents.allObjects forKey:kPCEvents];
    [defaults setObject:keyedActions.allObjects forKey:kPCActions];
    [defaults setObject:keyedConversions.allObjects forKey:kPCConversions];
    //
    [defaults synchronize];
    //
    PUBLISH([PCEventReportHistory new]);
}

@end
