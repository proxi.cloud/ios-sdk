//
//  ProxicloudSDK.h
//  Proxi.cloud SDK
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PCWifiManager.h"
#import "PCEnums.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
#import <tolo/Tolo.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "PCEvent.h"
#import "PCInternalEvents.h"
#import "PCResolver.h"

@interface PCWifiManager()

@property (nonatomic, strong) NSArray *interfacesNames;
@property (nonatomic, strong) PCMGetLayout *layout;
@property (nonatomic, strong) PCResolver *resolver;
@property (nonatomic, strong, nullable) NSString *currentlyConnctedWiFiSSID;
@end

@implementation PCWifiManager

- (instancetype)initWithResolver:(PCResolver *)resolver
{
    self = [super init];
    if (self) {
        _interfacesNames = (__bridge_transfer id)CNCopySupportedInterfaces();
        _resolver = resolver;
    }
    REGISTER();
    return self;
}

- (BOOL) isWiFiEnabled {
    NSCountedSet * cset = [NSCountedSet new];
    struct ifaddrs *interfaces;
    if( ! getifaddrs(&interfaces) ) {
        for( struct ifaddrs *interface = interfaces; interface; interface = interface->ifa_next) {
            if ((interface->ifa_flags & IFF_UP) == IFF_UP ) {
                [cset addObject:[NSString stringWithUTF8String:interface->ifa_name]];
            }
        }
    }
    return [cset countForObject:@"awdl0"] > 1 ? YES : NO;
}

- (void)scanForInterfacesWithSSID: (PCMWifiNetwork *)wifiNetwork {
    for (NSString *name in self.interfacesNames) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)name);
        NSString *foundSSID = info[@"SSID"];
        if ([foundSSID isEqualToString: [wifiNetwork.ssid stringByReplacingOccurrencesOfString:@"ssid_" withString:@""]]) {
            [self didConnectToWiFiNetwork: wifiNetwork];
        } else {
            [self notConnectedToWiFiNetwork: wifiNetwork];
        }
    }
}

- (void)notConnectedToWiFiNetwork:(PCMWifiNetwork *)network {
    if([self.currentlyConnctedWiFiSSID isEqualToString:network.ssid]) {
        NSLog(@"currently connected wifi ssid: %@", self.currentlyConnctedWiFiSSID);
        [self notConnectedToCurrentWifiNetwork];
    }
    return;
}

- (void)notConnectedToCurrentWifiNetwork {
    if(!isNull(self.currentlyConnctedWiFiSSID)) {
        PUBLISH(({
            PCEventDeviceDisconnectedFromWiFiNetwork *event = [PCEventDeviceDisconnectedFromWiFiNetwork new];
            PCMWifiNetwork *network = [[PCMWifiNetwork alloc] initWithSSID:self.currentlyConnctedWiFiSSID];
            event.wifi = network;
            event;
        }));
        self.currentlyConnctedWiFiSSID = nil;
    }
}

SUBSCRIBE(PCEventDeviceConnectedViaWAN) {
    [self notConnectedToCurrentWifiNetwork];
}

- (void)didConnectToWiFiNetwork: (PCMWifiNetwork *)wifiNetwork {
    if (self.currentlyConnctedWiFiSSID == nil) {
        self.currentlyConnctedWiFiSSID = wifiNetwork.ssid;
        [self publishDidConnectToWifi:wifiNetwork];
    } else if(self.currentlyConnctedWiFiSSID != nil && self.currentlyConnctedWiFiSSID != wifiNetwork.ssid) {
        PCMWifiNetwork *networkConnectedUntilNow = [PCMWifiNetwork new];
        networkConnectedUntilNow.ssid = self.currentlyConnctedWiFiSSID;
        [self notConnectedToWiFiNetwork: networkConnectedUntilNow];
        self.currentlyConnctedWiFiSSID = wifiNetwork.ssid;
        [self publishDidConnectToWifi:wifiNetwork];
    }
    return;
}

- (void)publishDidConnectToWifi:(PCMWifiNetwork *)network {
    PUBLISH(({
        PCEventDeviceConnectedToWiFiNetwork *event = [PCEventDeviceConnectedToWiFiNetwork new];
        event.wifi = network;
        event;
    }));
}

-(void)checkLayout:(PCMGetLayout *)layout {
    self.layout = layout;
    for (PCMAction *action in layout.actions) {
        [self fetchTriggerInAction:action];
    }
}

-(void)fetchTriggerInAction: (PCMAction *) action{
    for ( PCMTrigger *object in action.beacons) {
        if([object isKindOfClass: [PCMWifiNetwork class]]) {
            PCMWifiNetwork *wifi = (PCMWifiNetwork *) object;
            [self scanForInterfacesWithSSID: wifi];
        }
    }
}

- (PCWifiStatus)authorizationStatus {
    if([self isWiFiEnabled]) {
        return PCWifiOn;
    }
    return PCWifiOff;
}
- (void)fetchLayoutForEvents {
    if(isNull(self.layout)) {
        [_resolver requestLayoutForBeacon:nil trigger:kPCTriggerNone useCache:NO completion:^(PCMGetLayout * layout) {
            __weak typeof (self) weakSelf = self;
            [weakSelf checkLayout: layout];
        }];
    } else {
        [self checkLayout: self.layout];
    }
}

- (void)setLayout:(PCMGetLayout *)layout {
    _layout = layout;
}

SUBSCRIBE(PCEventDeviceConnectedViaWiFi) {
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray != nil){
        NSDictionary* myDict = (NSDictionary *) CFBridgingRelease(CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0)));
        if (myDict!=nil){
            NSString *connectedWifi = [myDict valueForKey:@"SSID"];
            if (connectedWifi != nil) {
                PUBLISH(({
                    PCEventDeviceConnectedToWiFiNetwork *event = [PCEventDeviceConnectedToWiFiNetwork new];
                    NSString *ssid = [@"ssid_" stringByAppendingString:connectedWifi];
                    event.wifi = [[PCMWifiNetwork alloc] initWithSSID: ssid];
                    event;
                }));
            }
        }
    }
    [self fetchLayoutForEvents];
}

@end
