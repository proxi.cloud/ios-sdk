//
//  main.m
//  ProxicloudDemoApp
//
//  Created by Bartosz Bartykowski on 28.09.2017.
//  Copyright © 2017 Proxi.cloud sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
