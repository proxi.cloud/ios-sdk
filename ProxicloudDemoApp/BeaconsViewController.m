
//  BeaconsViewController.m
//  Proxi.cloud SDK
//
//
//  Copyright (c) 2017 Proxi.cloud sp. z.o.o.. All rights reserved.
//

#import "BeaconsViewController.h"

//#import <ProxicloudSDK/ProxicloudSDK.h>
//#import <ProxicloudSDK/NSString+PCUUID.h>
#import "ProxicloudSDK.h"
#import "NSString+PCUUID.h"
#import <AdSupport/ASIdentifierManager.h>
@import UserNotifications;
#import <UserNotifications/UserNotifications.h>
#import <tolo/Tolo.h>
#import "PCUtilities.h"

#warning Enter your API key here
#define kAPIKey     @"6995ee2aa803a9d3730ae7b6375246d733c55e62ff7e41c47da13e59564568d1"

@interface BeaconsViewController () {
    NSMutableDictionary *beacons;
}

@end

@interface BeaconsViewController()<UNUserNotificationCenterDelegate>

@end

static NSString *const kReuseIdentifier = @"beaconCell";


@implementation BeaconsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    beacons = [NSMutableDictionary new];

    ASIdentifierManager *identifierManager = [ASIdentifierManager sharedManager];
    if ([identifierManager isAdvertisingTrackingEnabled]) {
        [[PCManager sharedManager] setIDFAValue:[identifierManager.advertisingIdentifier UUIDString]];
    }
    
    [[PCManager sharedManager] setApiKey:kAPIKey delegate:self];

    [[PCManager sharedManager] requestLocationAuthorization:YES];
    [[PCManager sharedManager] requestNotificationsAuthorization];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.title = @"Beacons";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return beacons.allValues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifier forIndexPath:indexPath];

    cell.imageView.image = nil;

    if ([beacons.allValues[indexPath.row] isKindOfClass:[PCMBeacon class]]) {
        PCMBeacon *beacon = beacons.allValues[indexPath.row];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Major:%i  Minor:%i", beacon.major, beacon.minor];


    } else if ([beacons.allValues[indexPath.row] isKindOfClass:[CBPeripheral class]]) {
        CBPeripheral *p = beacons.allValues[indexPath.row];

        cell.textLabel.text = p.name ? p.name : @"No Name";
        cell.detailTextLabel.text = p.identifier.UUIDString;

        cell.imageView.image = [self imageFromText:@"BLE"];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - ProxicloudSDK events

SUBSCRIBE(PCEventDeviceConnectedToWiFiNetwork) {
    //do stuff when connected to certain wifi network
    NSLog(@"📶📶📶📶📶📶📶📶📶📶📶📶📶 Connected to wifi network: %@ 📶📶📶📶📶📶📶📶📶📶📶📶📶", event.wifi.ssid);
}

#pragma mark PCEventLocationAuthorization
SUBSCRIBE(PCEventLocationAuthorization) {
    if (event.locationAuthorization==PCLocationAuthorizationStatusAuthorized) {
            [[PCManager sharedManager] startMonitoring];
    } else {
        NSLog(@"Location Service OFF, monitoring doesn't work");
    }
}

#pragma mark PCEventBluetoothAuthorization
SUBSCRIBE(PCEventBluetoothAuthorization) {
    // You only need to ask for Bluetooth authorisation when working with GATT
}

SUBSCRIBE(PCEventNotificationsAuthorization) {
    if (!event.notificationsAuthorization) {
//        When notifications are not allowed
    }
}

#pragma mark PCEventRegionEnter
SUBSCRIBE(PCEventRegionEnter) {
    [beacons setValue:event.beacon forKey:event.beacon.tid];
    [self.tableView reloadData];
    //
}

#pragma mark PCEventRegionExit
SUBSCRIBE(PCEventRegionExit) {
    [beacons setValue:nil forKey:event.beacon.tid];
    [self.tableView reloadData];
}

#pragma mark PCEventRangedBeacon
SUBSCRIBE(PCEventRangedBeacon) {
    [beacons setValue:event.beacon forKey:event.beacon.tid];
}

#pragma mark - Present Notification Alert

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(nonnull void (^)(UNNotificationPresentationOptions))completionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Notification alert!"
                                                                             message:@"This app just sent you a notification, do you want to see it?"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ignore = [UIAlertAction actionWithTitle:@"Ignore"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       // check if user decided to ignore notification
                                                   }];
    
    UIAlertAction *view = [UIAlertAction actionWithTitle:@"See"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                     [self takeActionWithLocalNotification:notification];
                                                 }];
    [alertController addAction:ignore];
    [alertController addAction:view];
    
    
    [self presentViewController:alertController animated:YES completion:^{
    }];
}

- (void)takeActionWithLocalNotification:(UNNotification *)localNotification {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:localNotification.request.content.title
                                                                             message:localNotification.request.content.body
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * _Nonnull action) {
                                                   // check if user saw this notification
                                               }];
    
    [alertController addAction:ok];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:^{
            if (localNotification.request.content.userInfo) {
                NSDictionary *dict = [localNotification.request.content.userInfo valueForKey:@"action"];
                 // print notification content
                NSLog(@"This is notification from Campaign!: %@", localNotification);
                
                PCMCampaignAction *action = [PCUtilities campaignActionFromDictionary:dict];
                if (action != nil) {
                    [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action.action copy]];
                }
            }
        }];
    });
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(nonnull UNNotificationResponse *)response withCompletionHandler:(nonnull void (^)(void))completionHandler {
    [self takeActionWithLocalNotification:response.notification];
}

#pragma mark -
-(UIImage *)imageFromText:(NSString *)text
{
    CGSize size  = [text sizeWithAttributes:nil];

    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);

    [text drawAtPoint:CGPointZero withAttributes:nil];

    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}


@end
