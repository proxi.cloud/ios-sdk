//
//  AppDelegate.h
//  ProxicloudDemoApp
//
//  Copyright © 2017 Proxi.cloud sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

