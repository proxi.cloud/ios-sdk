//
//  AppDelegate.m
//  ProxicloudDemoApp
//
//  Copyright © 2017 Proxi.cloud sp. z o.o. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import "PCUtilities.h"
#import "PCManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound + UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError *_Nullable error) {
                if (!granted) {
                    // print error content
                }
            }];
    
    

    return YES;
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    
    
    // print notification content
    NSLog(@"This is notification from Campaign!: %@", notification);
    NSString *action = nil;
    PCActionType actionType = Notification;
    if (notification.request.content.userInfo) {
        action = [PCEnums getActionUuid: notification.request.content.userInfo];
        actionType = [PCEnums getActionType: notification.request.content.userInfo];
    }
    UIAlertController *alertController;
    switch (actionType) {
        case Notification: {
                alertController = [UIAlertController alertControllerWithTitle:notification.request.content.title
                                                                      message:notification.request.content.body
                                                               preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *_Nonnull actionHandler) {
                                                               // report conversion: "notification clicked"
                                                               if (action) {
                                                                   [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
                                                               }
                                                           }];
                [alertController addAction:ok];
                [[self.window rootViewController] presentViewController:alertController animated:YES completion:^{
                }];
             
        }
        case Website:
        case DeepLink: {
            NSString *urlString = [notification.request.content.userInfo valueForKey:@"url"];
            NSURL *URL = [NSURL URLWithString:urlString];
                alertController = [UIAlertController alertControllerWithTitle:notification.request.content.title
                                                                      message:notification.request.content.body
                                                               preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *_Nonnull actionHandler) {
                                                               // report conversion: "notification clicked"
                                                               if (action) {
                                                                   [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
                                                               }
 
                                                                   [[UIApplication sharedApplication] openURL:URL options:@{}
                                                                      completionHandler:^(BOOL success) {
                                                                          
                                                                      }];
                                                           }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:nil];
                [alertController addAction:ok];
                [alertController addAction:cancel];
                
                [[self.window rootViewController] presentViewController:alertController animated:YES completion:^{
                }];
        }
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    
    
    // print notification content
    NSLog(@"This is notification from Campaign!: %@", response.notification);
    NSString *action = nil;
    PCActionType actionType = Notification;
    if (  response.notification.request.content.userInfo) {
        action = [PCEnums getActionUuid: response.notification.request.content.userInfo];
        actionType = [PCEnums getActionType: response.notification.request.content.userInfo];
    }
    switch (actionType) {
        case Notification: {
                [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
            break;
        }
        case Website:
        case DeepLink: {
            NSString *urlString = [response.notification.request.content.userInfo valueForKey:@"url"];
            NSURL *URL = [NSURL URLWithString: urlString];
                // report conversion: "notification clicked"
                if (action) {
                    [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
                }
                [[UIApplication sharedApplication] openURL:URL options:@{} completionHandler:nil];
        }
    }
}

    



//- (void)application:(UIApplication *)application didReceiveLocalNotification:(nonnull UILocalNotification *)notification {
//
//
//    // print notification content
//    NSLog(@"This is notification from Campaign!: %@", notification);
//    NSString *action = nil;
//    PCActionType actionType = Notification;
//    if (notification.userInfo) {
//        action = [PCEnums getActionUuid: notification.userInfo];
//        actionType = [PCEnums getActionType: notification.userInfo];
//    }
//    UIAlertController *alertController;
//    switch (actionType) {
//        case Notification: {
//            if (application.applicationState == UIApplicationStateActive) {
//
//                alertController = [UIAlertController alertControllerWithTitle:notification.alertTitle
//                                                                      message:notification.alertBody
//                                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
//                                                             style:UIAlertActionStyleDefault
//                                                           handler:^(UIAlertAction *_Nonnull actionHandler) {
//                                                               // report conversion: "notification clicked"
//                                                               if (action) {
//                                                                   [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
//                                                               }
//                                                           }];
//                [alertController addAction:ok];
//                [[self.window rootViewController] presentViewController:alertController animated:YES completion:^{
//                }];
//            } else if (action) {
//                [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
//            }
//            break;
//        }
//        case Website:
//        case DeepLink: {
//            NSString *urlString = [notification.userInfo valueForKey:@"url"];
//            NSURL *URL = [NSURL URLWithString:urlString];
//            if (application.applicationState == UIApplicationStateActive) {
//                alertController = [UIAlertController alertControllerWithTitle:notification.alertTitle
//                                                                      message:notification.alertBody
//                                                               preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
//                                                             style:UIAlertActionStyleDefault
//                                                           handler:^(UIAlertAction *_Nonnull actionHandler) {
//                                                               // report conversion: "notification clicked"
//                                                               if (action) {
//                                                                   [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
//                                                               }
//                                                               if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {  // iOS 10+
//                                                                   [application openURL:URL options:@{}
//                                                                      completionHandler:^(BOOL success) {
//
//                                                                      }];
//                                                               } else { // fallback to iOS 9.0
//                                                                   BOOL success = [application openURL:URL];
//                                                                   NSLog(@"Open %@: %d", urlString, success);
//                                                               }
//                                                           }];
//                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
//                                                                 style:UIAlertActionStyleCancel
//                                                               handler:^(UIAlertAction *_Nonnull action) {
//                                                               }];
//                [alertController addAction:ok];
//                [alertController addAction:cancel];
//
//                [[self.window rootViewController] presentViewController:alertController animated:YES completion:^{
//                }];
//                // when notification received in background
//            } else {
//                // report conversion: "notification clicked"
//                if (action) {
//                    [[PCManager sharedManager] reportConversion:kPCConversionSuccessful forCampaignAction:[action copy]];
//                }
//                if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {  // iOS 10+
//                    [application openURL:URL options:@{}
//                       completionHandler:^(BOOL success) {
//                           NSLog(@"Open %@: %d", urlString, success);
//                       }];
//                } else { // fallback to iOS 9.0
//                    BOOL success = [application openURL:URL];
//                    NSLog(@"Open %@: %d", urlString, success);
//                }
//            }
//            break;
//        }
//    }
//}



@end
