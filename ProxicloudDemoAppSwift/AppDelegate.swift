//
//  AppDelegate.swift
//  ProxicloudDemoAppSwift
//
//  Created by Bartek on 22.05.2018.
//  Copyright © 2018 Proxi.cloud sp. z o.o. All rights reserved.
//

import UIKit
import ProxicloudSDK
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        var action: String? = nil
        var actionType: PCActionType = .Notification
        if let userInfo = notification.request.content.userInfo as? [String : Any] {
            action = PCEnums.getActionUuid(userInfo)
            actionType = PCEnums.getActionType(userInfo)
        }
        switch actionType {
            case .Notification:
                let alertController = UIAlertController(title: notification.request.content.title, message: notification.request.content.body, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { alert in
                    // report conversion: "notification clicked"
                    if let action = action  {
                        PCManager.shared().report(.successful, forCampaignAction: action)
                    }
                })
                alertController.addAction(okAction)
                self.window?.rootViewController?.present(alertController, animated: true)
            case .Website, .DeepLink:
                guard let stringUrl = notification.request.content.userInfo["url"] as? String,
                let url = URL(string: stringUrl) else {
                    return
                }
                let alertController = UIAlertController(title: notification.request.content.title, message: notification.request.content.body, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                    // report conversion: "notification clicked"
                    if let action = action {
                        PCManager.shared().report(.successful, forCampaignAction: action)
                    }
                    UIApplication.shared.open(url)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.window?.rootViewController?.present(alertController, animated: true)
        }
    }

//    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
//
//        var action: String? = nil
//        var actionType: PCActionType = .Notification
//        if let userInfo = notification.userInfo as? [String : Any] {
//            action = PCEnums.getActionUuid(userInfo)
//            actionType = PCEnums.getActionType(userInfo)
//        }
//
//        switch actionType {
//        case .Notification:
//            if application.applicationState == .active {
//                let alertController = UIAlertController(title: notification.alertTitle, message: notification.alertBody, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "OK", style: .default, handler: { alert in
//                    // report conversion: "notification clicked"
//                    if let action = action  {
//                        PCManager.shared().report(.successful, forCampaignAction: action)
//                    }
//                })
//                alertController.addAction(okAction)
//                self.window?.rootViewController?.present(alertController, animated: true)
//            } else if let action = action {
//                PCManager.shared().report(.successful, forCampaignAction: action)
//            }
//        case .Website, .DeepLink:
//            guard let stringUrl = notification.userInfo?["url"] as? String else {
//                return
//            }
//            guard let url = URL(string: stringUrl) else {
//                return
//            }
//            if application.applicationState == .active {
//                let alertController = UIAlertController(title: notification.alertTitle, message: notification.alertBody, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "OK", style: .default) { _ in
//                    // report conversion: "notification clicked"
//                    if let action = action {
//                        PCManager.shared().report(.successful, forCampaignAction: action)
//                    }
//                    if #available(iOS 10.0, *) {
//                        UIApplication.shared.open(url)
//                    } else {
//                        UIApplication.shared.openURL(url)
//                    }
//                }
//                alertController.addAction(okAction)
//                self.window?.rootViewController?.present(alertController, animated: true)
//            } else {
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//            break
//        }
//    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        PCManager.shared().setApiKey("6995ee2aa803a9d3730ae7b6375246d733c55e62ff7e41c47da13e59564568d1", delegate: self)
        PCManager.shared().requestLocationAuthorization(true)

        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            guard error == nil else {
                // print error content here
                return
            }
        }

        return true
    }

    @objc public func onPCEventLocationAuthorization(_ event: PCEventLocationAuthorization) {
        if event.locationAuthorization == .authorized {
            PCManager.shared().startMonitoring()
        }
    }

    
    // This function will be called right after user tap on the notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        var action: String? = nil
        var actionType: PCActionType = .Notification
        if let userInfo = response.notification.request.content.userInfo as? [String : Any] {
            action = PCEnums.getActionUuid(userInfo)
            actionType = PCEnums.getActionType(userInfo)
        }
        
        switch actionType {
        case .Notification:
                PCManager.shared().report(.successful, forCampaignAction: action)
        case .Website, .DeepLink:
            guard let stringUrl = response.notification.request.content.userInfo["url"] as? String,
            let url = URL(string: stringUrl) else {
                return
            }
            PCManager.shared().report(.successful, forCampaignAction: action)
            UIApplication.shared.open(url)
        }
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}
