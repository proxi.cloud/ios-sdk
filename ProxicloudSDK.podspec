Pod::Spec.new do |s|
s.name             = 'ProxicloudSDK'
s.version          = '2.4.2'
s.summary          = 'iOS SDK for handling iBeacon technology via the Proxi Cloud Beacon Management Platform.'

s.description      = <<-DESC
SDK features
Scanning for Bluetooth beacons and reacting accordingly when detecting one correlated with an action defined in the proxi.cloud panel
Observing rough location for detecting presence inside a defined geofence. Reacting accordingly to actions created in the proxi.cloud panel
Scanning for WiFi networks - reacting if a detected network is connected to an action in the proxi.cloud panel
Reacting and executing actions set in the proxi.cloud panel based if the user belongs to a defined segment.

More details at https://developer.proxi.cloud
DESC

s.homepage         = 'https://developer.proxi.cloud/ios/'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Proxi Cloud' => 'support@proxi.cloud' }
s.source           = { :git => 'https://gitlab.com/proxi.cloud/ios-sdk.git', :tag => '2.4.2' }

s.ios.deployment_target = '10.0'

s.source_files = 'ProxicloudSDK/**/**/*.{h,m}'
s.static_framework = true
s.dependency 'JSONModel', '~> 1.1'
s.dependency 'tolo'
s.dependency 'UICKeyChainStore', '~> 2.0'
s.dependency 'ObjCGeoHash'
s.dependency 'Firebase'
s.dependency 'FirebaseInstanceID'
s.dependency 'FirebaseMessaging'
end

